package processor;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import command.CommandExecuter;
import entity.AudioQuality;
import entity.FileData;
import model.YoutubeDlCommandArguments;
import util.AudioQualityComparator;
import util.DateUtils;
import util.FileUtils;
import util.RegexpUtil;

public class AudioDataProcessor {

	private static final String TITLE_TEMPLATE = "%(title)s";
	private static final String EXTENTION_TEMPLATE = "%(ext)s";

	// TODO: Add logging

	public void process(FileData fileData) {
		defineBestAudio(fileData);
		downloadBestAudio(fileData);
	}

	private void defineBestAudio(FileData fileData) {
		List<AudioQuality> audioQualityList = fileData.getAudioQualityList();
		audioQualityList.sort(new AudioQualityComparator());
		Collections.reverse(audioQualityList);

		// get 2 best audio quality
		List<AudioQuality> bestAudioQualityList = new ArrayList<AudioQuality>(2);

		AudioQuality audioQuality1 = audioQualityList.get(0);
		AudioQuality audioQuality2 = audioQualityList.get(1);

		if (audioQuality1.isSameAudioQuality(audioQuality2)) {
			bestAudioQualityList.add(audioQuality1);
		} else {
			boolean isAudioOnlyFormat1 = audioQuality1.isAudioOnlyFormat();
			boolean isAudioOnlyFormat2 = audioQuality2.isAudioOnlyFormat();

			if (isAudioOnlyFormat1 && isAudioOnlyFormat2) {
				bestAudioQualityList.add(audioQuality1);
			} else if (!isAudioOnlyFormat1 && !isAudioOnlyFormat2) {
				bestAudioQualityList.add(audioQuality1);
			} else if (isAudioOnlyFormat1 && !isAudioOnlyFormat2) {
				bestAudioQualityList.add(audioQuality1);
			} else if (!isAudioOnlyFormat1 && isAudioOnlyFormat2) {
				bestAudioQualityList.add(audioQuality1);
				bestAudioQualityList.add(audioQuality2);
			}
		}
		// leave only required best quality
		fileData.setAudioQualityList(bestAudioQualityList);
	}

	private void downloadBestAudio(FileData fileData) {
		CommandExecuter commandExecuter = new CommandExecuter();

		String videoURL = fileData.getVideoUrl();

		YoutubeDlCommandArguments youtubeDlCommandArguments = new YoutubeDlCommandArguments();
		youtubeDlCommandArguments.setUrl(videoURL);

		String downloadPath = fileData.getDownloadAudioFolderPath();
		youtubeDlCommandArguments.setDownloadPath(downloadPath);

		Date uploadDate = fileData.getUploadDate();
		String title = fileData.getTitle();
		String downloadFileNameTemplate = prepareFileNameTemplate(title, uploadDate);
		youtubeDlCommandArguments.setFileNameTemplate(downloadFileNameTemplate);

		List<AudioQuality> audioQualityList = fileData.getAudioQualityList();

		int audioQualityListSize = audioQualityList.size();
		boolean isComparisonRequired = (audioQualityListSize > 1);

		for (int index = 0; index < audioQualityListSize; index++) {
			AudioQuality audioQuality = audioQualityList.get(index);
			audioQuality.setDownloadIndex(index);

			Integer formatCode = audioQuality.getFormatCode();
			youtubeDlCommandArguments.setFormatCode(formatCode);

			youtubeDlCommandArguments.setDownloadIndex(index);

			commandExecuter.downloadAudio(youtubeDlCommandArguments);
			// TODO: Include other audio info: artist, album, URL etc.
			storeRealAudioFileName(fileData, index, isComparisonRequired);
		}
		fileData.setDownloadAudioFolderPath(downloadPath);

		if (isComparisonRequired) {
			defineRealAudioQuality(fileData, downloadPath);
			leaveOnlyBestQualityAudio(fileData, downloadPath);
		}

		ImageDataProcessor imageDataProcessor = new ImageDataProcessor();
		imageDataProcessor.setCoverImage(fileData);
	}

	private void storeRealAudioFileName(FileData fileData, int downloadIndex, boolean isComparisonRequired) {
		String downloadAudioFolderPath = fileData.getDownloadAudioFolderPath();

		File downloadFolder = new File(downloadAudioFolderPath);
		File[] listFiles = downloadFolder.listFiles();

		for (File file : listFiles) {
			if (file.isFile() && !file.isHidden()) {
				String fileName = file.getName();

				if (fileName.startsWith(downloadIndex + "_")) {
					String bestAudioPrefix = downloadIndex + "_";

					if (RegexpUtil.isAudioToCompareFileName(fileName)) {
						if (fileName.startsWith(bestAudioPrefix)) {
							int prefixBeginIndex = bestAudioPrefix.length();
							int prefixEndIndex = fileName.length();
							String origName = fileName.substring(prefixBeginIndex, prefixEndIndex);

							fileData.setDownloadAudioFileName(origName);

							if (!isComparisonRequired) {
								String fileToRenameFullPath = fileData.getDownloadAudioFullPath();
								String currentFileFullPath = file.getAbsolutePath();

								FileUtils.renameFile(currentFileFullPath, fileToRenameFullPath);
							}
						}
					}
				}
			}
		}
	}

	private String prepareFileNameTemplate(String title, Date uploadDate) {
		String uploadDateStr = DateUtils.dateToDDMMYYYY(uploadDate);

		StringBuilder nameTemplateSb = new StringBuilder();
		nameTemplateSb.append(TITLE_TEMPLATE);

		boolean isTitleContainsUploadDateDate = DateUtils.isStringContainsDate(title, uploadDate);
		if (!isTitleContainsUploadDateDate) {
			nameTemplateSb.append(" ");
			nameTemplateSb.append("(");
			nameTemplateSb.append(uploadDateStr);
			nameTemplateSb.append(")");
		}
		nameTemplateSb.append(".");
		nameTemplateSb.append(EXTENTION_TEMPLATE);

		return nameTemplateSb.toString();
	}

	private void defineRealAudioQuality(FileData fileData, String downloadPath) {
		List<AudioQuality> audioQualityList = fileData.getAudioQualityList();

		File dowloadFolder = new File(downloadPath);
		File[] listFiles = dowloadFolder.listFiles();

		for (AudioQuality audioQuality : audioQualityList) {
			Integer downloadIndex = audioQuality.getDownloadIndex();

			for (File file : listFiles) {
				if (file.isFile() && !file.isHidden()) {
					String fileName = file.getName();

					if (fileName.startsWith(downloadIndex + "_")) {
						CommandExecuter commandExecuter = new CommandExecuter();
						String absolutePath = file.getAbsolutePath();
						List<String> audioInfoRawList = commandExecuter.getAudioInfo(absolutePath);

						MediaInfoParser mediaInfoParser = new MediaInfoParser();
						int realBitrate = mediaInfoParser.parseFFProbeAudioRawData(audioInfoRawList);
						audioQuality.setBitrate(realBitrate);
					}
				}
			}
		}
	}

	private void leaveOnlyBestQualityAudio(FileData fileData, String downloadPath) {
		List<AudioQuality> audioQualityList = fileData.getAudioQualityList();
		audioQualityList.sort(new AudioQualityComparator());

		int size = audioQualityList.size();
		AudioQuality bestAudioQuality = audioQualityList.get(size - 1);
		Integer bestAudioDownloadIndex = bestAudioQuality.getDownloadIndex();

		File downloadFolder = new File(downloadPath);
		File[] listFiles = downloadFolder.listFiles();

		for (File file : listFiles) {
			if (file.isFile() && !file.isHidden()) {
				String fileName = file.getName();
				String bestAudioPrefix = bestAudioDownloadIndex + "_";

				if (RegexpUtil.isAudioToCompareFileName(fileName)) {
					if (fileName.startsWith(bestAudioPrefix)) {
						// rename file
						String fileToRenameFullPath = fileData.getDownloadAudioFullPath();
						String currentFileFullPath = file.getAbsolutePath();

						FileUtils.renameFile(currentFileFullPath, fileToRenameFullPath);
					} else {
						// delete file
						boolean deleteResult = file.delete();
						if (!deleteResult) {
							System.out.println("Can't delete audio file with lower quality!");
						}
					}
				}
			}
		}
	}

}

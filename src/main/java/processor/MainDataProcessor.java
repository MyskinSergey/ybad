package processor;

import java.io.IOException;
import java.util.Collection;

import command.CommandExecuter;
import entity.FileData;
import entity.raw.json.VideoInfoJsonObject;
import model.YoutubeDlCommandArguments;
import options.LatinEncodingCaseEnum;
import util.FileUtils;
import util.Logger;

public class MainDataProcessor {

	private final int MAX_TRY_COUNT = 5;

	public FileData getFileInfo(String videoUrl) {
		YoutubeDlCommandArguments youtubeDlCommandArguments = getCommandArguments(videoUrl);

		CommandExecuter commandExecuter = new CommandExecuter();
		String videoJsonInfo = commandExecuter.getVideoJsonInfo(youtubeDlCommandArguments);

		RawInfoDataSeparator rawInfoDataSeparator = new RawInfoDataSeparator();
		VideoInfoJsonObject separatedRawData = null;

		for (int i = 0; i < MAX_TRY_COUNT; i++) {
			try {
				separatedRawData = rawInfoDataSeparator.fillRawData(videoJsonInfo);
			} catch (IOException ex) {
				Logger.error(ex);
				ex.printStackTrace();

				Logger.info("Error occurs, try again, attempt № " + i);
				continue;
			}
			break;
		}

		if (separatedRawData != null) {
			RawInfoDataProcessor rawInfoDataProcessor = new RawInfoDataProcessor();
			FileData fileData = rawInfoDataProcessor.process(separatedRawData);
			return fileData;
		} else {
			return null;
		}
	}

	public FileData getFileInfo(String channelOrPlaylistUrl, int fileIndex) {
		YoutubeDlCommandArguments youtubeDlCommandArguments = getCommandArguments(channelOrPlaylistUrl, fileIndex,
				fileIndex);

		CommandExecuter commandExecuter = new CommandExecuter();
		String videoJsonInfo = commandExecuter.getVideoJsonInfo(youtubeDlCommandArguments);

		RawInfoDataSeparator rawInfoDataSeparator = new RawInfoDataSeparator();
		VideoInfoJsonObject separatedRawData = null;

		for (int i = 0; i < MAX_TRY_COUNT; i++) {
			try {
				separatedRawData = rawInfoDataSeparator.fillRawData(videoJsonInfo);
			} catch (IOException ex) {
				Logger.error(ex);
				ex.printStackTrace();

				Logger.info("Error occurs, try again, attempt № " + i);
				continue;
			}
			break;
		}

		if (separatedRawData != null) {
			RawInfoDataProcessor rawInfoDataProcessor = new RawInfoDataProcessor();
			FileData fileData = rawInfoDataProcessor.process(separatedRawData);
			return fileData;
		} else {
			return null;
		}
	}

	public void downloadBestAudio(Collection<FileData> fileDataCollection, String downloadPath) {
		boolean isLatinSymbolsOnlyWindowsPath = FileUtils.isLatinSymbolsOnlyWindowsPath(downloadPath);
		for (FileData fileData : fileDataCollection) {
			defineEncodingCase(fileData, isLatinSymbolsOnlyWindowsPath);
			fileData.setDownloadAudioFolderPath(downloadPath);

			downloadBestAudio(fileData, downloadPath);
		}
	}

	private void defineEncodingCase(FileData fileData, boolean isLatinSymbolsOnlyWindowsPath) {
		String title = fileData.getTitle();
		boolean isLatinSymbolsOnlyFileName = FileUtils.isLatinSymbolsOnlyFileName(title);

		LatinEncodingCaseEnum latinEncodingCase;

		if (isLatinSymbolsOnlyWindowsPath && isLatinSymbolsOnlyFileName) {
			latinEncodingCase = LatinEncodingCaseEnum.DOWNLOAD_PATH_AND_FILE_NAME;
		} else if (isLatinSymbolsOnlyWindowsPath) {
			latinEncodingCase = LatinEncodingCaseEnum.DOWNLOAD_PATH_ONLY;
		} else if (isLatinSymbolsOnlyFileName) {
			latinEncodingCase = LatinEncodingCaseEnum.FILE_NAME_ONLY;
		} else {
			latinEncodingCase = LatinEncodingCaseEnum.NO_ITEMS;
		}
		fileData.setLatinEncodingCase(latinEncodingCase);
	}

	public void downloadBestAudio(FileData fileData, String downloadPath) {
		LatinEncodingCaseEnum latinEncodingCase = fileData.getLatinEncodingCase();
		if (latinEncodingCase == null) {
			boolean isLatinSymbolsOnlyWindowsPath = FileUtils.isLatinSymbolsOnlyWindowsPath(downloadPath);
			defineEncodingCase(fileData, isLatinSymbolsOnlyWindowsPath);
		}
		fileData.setDownloadAudioFolderPath(downloadPath);

		AudioDataProcessor audioDataProcessor = new AudioDataProcessor();
		audioDataProcessor.process(fileData);
	}

	private YoutubeDlCommandArguments getCommandArguments(String url) {
		YoutubeDlCommandArguments youtubeDlCommandArguments = new YoutubeDlCommandArguments();
		youtubeDlCommandArguments.setUrl(url);

		return youtubeDlCommandArguments;
	}

	private YoutubeDlCommandArguments getCommandArguments(String url, int startIndex, int endIndex) {
		YoutubeDlCommandArguments youtubeDlCommandArguments = new YoutubeDlCommandArguments();
		youtubeDlCommandArguments.setUrl(url);
		youtubeDlCommandArguments.setStartIndex(startIndex);
		youtubeDlCommandArguments.setEndIndex(endIndex);

		return youtubeDlCommandArguments;
	}

}

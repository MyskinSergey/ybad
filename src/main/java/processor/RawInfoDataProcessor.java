package processor;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import entity.AudioQuality;
import entity.FileData;
import entity.raw.json.FormatJsonObject;
import entity.raw.json.VideoInfoJsonObject;
import options.AudioContainerEnum;
import options.AudioFormatEnum;
import options.FormatTagEnum;
import util.DateUtils;

public class RawInfoDataProcessor {

	public FileData process(VideoInfoJsonObject videoInfoRawData) {
		FileData fileData = new FileData();

		processCommonFileInfo(fileData, videoInfoRawData);
		processAudioQualityInfo(fileData, videoInfoRawData);

		return fileData;
	}

	private void processCommonFileInfo(FileData fileData, VideoInfoJsonObject videoInfoRawData) {
		// TODO : set video index
		// fileData.setIndex(fileIndex);

		String fulltitle = videoInfoRawData.getFulltitle();
		fileData.setTitle(fulltitle);

		String webpageUrl = videoInfoRawData.getWebpage_url();
		fileData.setVideoUrl(webpageUrl);

		Integer durationInSeconds = videoInfoRawData.getDuration();
		String durationAsHHMMSS = DateUtils.getDurationAsHHMMSS(durationInSeconds);
		fileData.setDuration(durationAsHHMMSS);

		String thumbnailImageUrl = videoInfoRawData.getThumbnail();
		fileData.setThumbnailImageUrl(thumbnailImageUrl);

		String uploadDateStr = videoInfoRawData.getUpload_date();
		Date uploadDate = DateUtils.parseUploadDate(uploadDateStr);
		fileData.setUploadDate(uploadDate);

		// TODO: Get playlistUrl & playlistName in some way

		String uploader = videoInfoRawData.getUploader();
		fileData.setChannelName(uploader);

		String uploaderUrl = videoInfoRawData.getUploader_url();
		fileData.setChannelUrl(uploaderUrl);
	}

	private void processAudioQualityInfo(FileData fileData, VideoInfoJsonObject videoInfoRawData) {
		List<FormatJsonObject> allFormats = videoInfoRawData.getFormats();
		List<AudioQuality> audioQualityList = new LinkedList<AudioQuality>();

		for (FormatJsonObject formatJsonObject : allFormats) {
			String formatNote = formatJsonObject.getFormat_note();

			boolean isAudioOnly = false;

			String acodecJsonStr = formatJsonObject.getAcodec();
			// TODO: Move "none" to constants
			boolean isContainsAudio = !acodecJsonStr.equals("none");

			if (isContainsAudio) {
				String onlyAudioFormatTag = FormatTagEnum.DASH_AUDIO.getTag();
				if (formatNote.equals(onlyAudioFormatTag)) {
					isAudioOnly = true;
				}
			}

			if (isContainsAudio) {
				AudioQuality audioQuality = new AudioQuality();

				// set format id
				Integer formatId = formatJsonObject.getFormat_id();
				audioQuality.setFormatCode(formatId);

				// set video extension
				String ext = formatJsonObject.getExt();
				audioQuality.setVideoExtension(ext);

				// set audio format
				String audioCodecRawStr = acodecJsonStr.trim();
				String audioCodec;
				if (audioCodecRawStr.contains(".")) {
					String[] split = audioCodecRawStr.split("\\.");
					audioCodec = split[0];
				} else {
					audioCodec = audioCodecRawStr;
				}
				String audioCodecUpperCase = audioCodec.toUpperCase();
				AudioContainerEnum audioContainer = AudioContainerEnum.valueOf(audioCodecUpperCase);
				AudioFormatEnum audioFormat = audioContainer.getAudioFormat();
				audioQuality.setAudioFormat(audioFormat);

				// set bitrate
				Integer abr = formatJsonObject.getAbr();
				audioQuality.setBitrate(abr);

				// set size
				Long filesize = formatJsonObject.getFilesize();
				audioQuality.setSize(filesize);

				// set is audio only flag
				audioQuality.setAudioOnlyFormat(isAudioOnly);

				audioQualityList.add(audioQuality);
			}
		}
		fileData.setAudioQualityList(audioQualityList);
	}

}

package processor;

import java.io.File;

import command.CommandExecuter;
import config.ConfigUtils;
import entity.FileData;
import model.FFMpegChangeImageArguments;
import model.Kid3SetCoverArguments;
import options.LatinEncodingCaseEnum;
import options.ffmpeg.FFMpegColorEnum;
import util.FileUtils;

public class ImageDataProcessor {

	// TODO: Set right extension to created cover file (same as source image)
	public static final String COVER_IMAGE_FILE_NAME = "Cover.jpg";
	public static final String TEMP_AUDIO_FILE_NAME_TEMPLATE = "audio.%s";

	public void setCoverImage(FileData fileData) {
		FFMpegChangeImageArguments ffMpegCommandArguments = createCoverImage(fileData);

		Kid3SetCoverArguments kid3SetCoverArguments = doPreInsertCoverActions(fileData, ffMpegCommandArguments);
		insertCoverImage(kid3SetCoverArguments);
		doPostInsertCoverActions(fileData, kid3SetCoverArguments);
	}

	private FFMpegChangeImageArguments createCoverImage(FileData fileData) {
		FFMpegChangeImageArguments ffMpegCommandArguments = prepareFFMpegCommandArguments(fileData);

		CommandExecuter commandExecuter = new CommandExecuter();
		commandExecuter.createCoverImage(ffMpegCommandArguments);

		return ffMpegCommandArguments;
	}

	private void insertCoverImage(Kid3SetCoverArguments kid3SetCoverArguments) {
		CommandExecuter commandExecuter = new CommandExecuter();
		commandExecuter.setCoverImage(kid3SetCoverArguments);
	}

	private Kid3SetCoverArguments doPreInsertCoverActions(FileData fileData,
			FFMpegChangeImageArguments ffMpegCommandArguments) {
		// TODO: Solve problem with path that contains cyrillic symbols
		String coverFilePath = ffMpegCommandArguments.getDestinationCoverFilePath();

		String downloadAudioFullPath = fileData.getDownloadAudioFullPath();
		String downloadAudioFileName = fileData.getDownloadAudioFileName();

		Kid3SetCoverArguments kid3SetCoverArguments = new Kid3SetCoverArguments();
		LatinEncodingCaseEnum latinEncodingCase = fileData.getLatinEncodingCase();

		switch (latinEncodingCase) {
		case DOWNLOAD_PATH_AND_FILE_NAME:
			kid3SetCoverArguments.setSourceImagePath(coverFilePath);
			kid3SetCoverArguments.setSourceAudioPath(downloadAudioFullPath);

			break;
		case DOWNLOAD_PATH_ONLY:
			kid3SetCoverArguments.setSourceImagePath(coverFilePath);

			String tempAudioFilePath = renameAudioBeforeSetCover(fileData);
			kid3SetCoverArguments.setSourceAudioPath(tempAudioFilePath);

			break;
		case FILE_NAME_ONLY:
			File systemTempFolder = FileUtils.getSystemTempFolder();
			String systemTempFolderPath = systemTempFolder.getAbsolutePath();

			String tempFolderCoverPath = systemTempFolderPath + File.separator + COVER_IMAGE_FILE_NAME;
			String tempFolderAudioPath = systemTempFolderPath + File.separator + downloadAudioFileName;

			FileUtils.moveFile(coverFilePath, tempFolderCoverPath);
			FileUtils.moveFile(downloadAudioFullPath, tempFolderAudioPath);

			kid3SetCoverArguments.setSourceImagePath(tempFolderCoverPath);
			kid3SetCoverArguments.setSourceAudioPath(tempFolderAudioPath);
			break;
		case NO_ITEMS:
			File systemTempFolder2 = FileUtils.getSystemTempFolder();
			String systemTempFolderPath2 = systemTempFolder2.getAbsolutePath();

			String fileExtension = FileUtils.getFileExtension(downloadAudioFullPath);
			String tempAudioFileName = String.format(TEMP_AUDIO_FILE_NAME_TEMPLATE, fileExtension);

			String tempFolderCoverPath2 = systemTempFolderPath2 + File.separator + COVER_IMAGE_FILE_NAME;
			String tempFolderAudioPath2 = systemTempFolderPath2 + File.separator + tempAudioFileName;

			FileUtils.moveFile(coverFilePath, tempFolderCoverPath2);
			FileUtils.moveFile(downloadAudioFullPath, tempFolderAudioPath2);

			kid3SetCoverArguments.setSourceImagePath(tempFolderCoverPath2);
			kid3SetCoverArguments.setSourceAudioPath(tempFolderAudioPath2);

			break;
		default:
			break;
		}
		return kid3SetCoverArguments;
	}

	private void doPostInsertCoverActions(FileData fileData, Kid3SetCoverArguments kid3SetCoverArguments) {

		String downloadAudioFullPath = fileData.getDownloadAudioFullPath();

		String sourceImagePath = kid3SetCoverArguments.getSourceImagePath();
		String sourceAudioPath = kid3SetCoverArguments.getSourceAudioPath();

		LatinEncodingCaseEnum latinEncodingCase = fileData.getLatinEncodingCase();

		switch (latinEncodingCase) {
		case DOWNLOAD_PATH_AND_FILE_NAME:
			break;
		case DOWNLOAD_PATH_ONLY:
			FileUtils.renameFile(sourceAudioPath, downloadAudioFullPath);

			break;
		case FILE_NAME_ONLY:
			FileUtils.moveFile(sourceAudioPath, downloadAudioFullPath);

			break;
		case NO_ITEMS:
			FileUtils.moveFile(sourceAudioPath, downloadAudioFullPath);

			break;
		default:
			break;
		}
		FileUtils.removeFile(sourceImagePath);
	}

	private String renameAudioBeforeSetCover(FileData fileData) {
		String downloadAudioFullPath = fileData.getDownloadAudioFullPath();
		String downloadAudioFolderPath = fileData.getDownloadAudioFolderPath();

		String fileExtension = FileUtils.getFileExtension(downloadAudioFullPath);
		String tempAudioFileName = String.format(TEMP_AUDIO_FILE_NAME_TEMPLATE, fileExtension);

		String tempAudioFilePath = downloadAudioFolderPath + File.separator + tempAudioFileName;

		FileUtils.renameFile(downloadAudioFullPath, tempAudioFilePath);
		return tempAudioFilePath;
	}

	private FFMpegChangeImageArguments prepareFFMpegCommandArguments(FileData fileData) {

		Integer imageWidth = fileData.getThumbnailImageWidth();
		Integer imageHeight = fileData.getThumbnailImageHeight();

		if ((imageWidth != null) && (imageHeight != null)) {
			if ((imageWidth != ConfigUtils.FAKE_ID) && (imageHeight != ConfigUtils.FAKE_ID)) {
				FFMpegChangeImageArguments ffMpegArguments = new FFMpegChangeImageArguments();

				String thumbnailImageUrl = fileData.getThumbnailImageUrl();
				ffMpegArguments.setSourceUrl(thumbnailImageUrl);

				String downloadFileFolderPath = fileData.getDownloadAudioFolderPath();
				ffMpegArguments.setDestinationCoverFileFolderPath(downloadFileFolderPath);

				String destinationPath = downloadFileFolderPath + File.separator + COVER_IMAGE_FILE_NAME;
				ffMpegArguments.setDestinationCoverFilePath(destinationPath);

				ffMpegArguments.setScaleWidth(imageWidth);
				ffMpegArguments.setScaleHeight(imageHeight);

				if (imageWidth > imageHeight) {
					ffMpegArguments.setPadWidth(imageWidth);
					ffMpegArguments.setPadHeight(imageWidth);

					int whDiff = (imageWidth - imageHeight);
					int totalBandHalfHeight = (int) Math.round((double) whDiff / (double) 2);

					ffMpegArguments.setPadX(0);
					ffMpegArguments.setPadY(totalBandHalfHeight);
				} else {
					ffMpegArguments.setPadWidth(imageHeight);
					ffMpegArguments.setPadHeight(imageHeight);

					int whDiff = (imageHeight - imageWidth);
					int totalBandHalfWidth = (int) Math.round((double) whDiff / (double) 2);

					ffMpegArguments.setPadX(totalBandHalfWidth);
					ffMpegArguments.setPadY(0);
				}
				ffMpegArguments.setPadColor(FFMpegColorEnum.BLACK.getName());

				return ffMpegArguments;
			}
		}
		return null;
	}

}

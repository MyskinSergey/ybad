package processor;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import entity.raw.json.VideoInfoJsonObject;

public class RawInfoDataSeparator {

	public VideoInfoJsonObject fillRawData(String jsonInfo)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		VideoInfoJsonObject videoInfo = null;
		videoInfo = objectMapper.readValue(jsonInfo, VideoInfoJsonObject.class);

		return videoInfo;
	}

}

package processor;

import java.util.List;

import util.RegexpUtil;

public class MediaInfoParser {

	public int parseFFProbeAudioRawData(List<String> rawAudioInfo) {
		for (String line : rawAudioInfo) {
			if (line.contains("bitrate")) {
				Integer bitRate = RegexpUtil.getBitRate(line);
				return bitRate;
			}
		}
		return 0;
	}

}

package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entity.FileData;
import util.DateUtils;
import util.Logger;
import view.util.ImageUtils;

public class VideoInfoComponent extends JPanel {

	private static final long serialVersionUID = -4645122547100732991L;

	private JCheckBox checkBox;

	public VideoInfoComponent(FileData fileData) {
		super();

		setLayout(new BorderLayout());
		setBorder(new EmptyBorder(10, 0, 10, 0));

		initView(fileData);
	}

	private void initView(FileData fileData) {

		JPanel leftPanelPanel = new JPanel(new FlowLayout());

		checkBox = new JCheckBox();
		leftPanelPanel.add(checkBox);

		// add thumbnail image
		String thumbnailImageUrl = fileData.getThumbnailImageUrl();
		JLabel thumbnailImageLabel = null;
		try {
			URL thumbnailImageURL = new URL(thumbnailImageUrl);
			ImageIcon thumbnailImageIcon = new ImageIcon(thumbnailImageURL);
			
			int imageWidth = thumbnailImageIcon.getIconWidth();
			int imageHeight = thumbnailImageIcon.getIconHeight();
			fileData.setThumbnailImageWidth(imageWidth);
			fileData.setThumbnailImageHeight(imageHeight);
			
			ImageIcon thumbnailScaledIcon = createThumbnailScaledIcon(thumbnailImageIcon);
			thumbnailImageLabel = new JLabel(thumbnailScaledIcon);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			Logger.error(e);

			// TODO: Process adding image
		}
		leftPanelPanel.add(thumbnailImageLabel);

		add(leftPanelPanel, BorderLayout.WEST);

		JPanel centerVideoInfoPanel = new JPanel(new GridLayout(3, 1));
		centerVideoInfoPanel.setBorder(new EmptyBorder(0, 10, 0, 20));

		// add video title
		String title = fileData.getTitle();
		JLabel titleLabel = new JLabel(title);
		centerVideoInfoPanel.add(titleLabel);

		// add channel name
		String channelName = fileData.getChannelName();
		JLabel channelLabel = new JLabel(channelName);
		centerVideoInfoPanel.add(channelLabel);

		// add video URL
		String videoUrl = fileData.getVideoUrl();
		JLabel videoURLlabel = new JLabel(videoUrl);
		centerVideoInfoPanel.add(videoURLlabel);

		add(centerVideoInfoPanel, BorderLayout.CENTER);

		JPanel rightPanel = new JPanel(new GridLayout(3, 1));

		// add duration
		String duration = fileData.getDuration();
		JLabel durationLabel = new JLabel(duration);
		rightPanel.add(durationLabel);

		// add stub label
		JLabel stubLabel = new JLabel("");
		rightPanel.add(stubLabel);

		// add video date
		Date uploadDate = fileData.getUploadDate();
		String dateStr = DateUtils.dateToDDMMYYYY(uploadDate);
		JLabel dateLabel = new JLabel(dateStr);
		rightPanel.add(dateLabel);

		add(rightPanel, BorderLayout.EAST);
	}

	private ImageIcon createThumbnailScaledIcon(ImageIcon thumbnailImageIcon) throws MalformedURLException {
		Image thumbnailImage = thumbnailImageIcon.getImage();
		Image thumbnailScaledImage = ImageUtils.getScaledImage(thumbnailImage, 120, 90);
		ImageIcon thumbnailScaledIcon = new ImageIcon(thumbnailScaledImage);

		return thumbnailScaledIcon;
	}

	public boolean isSelected() {
		boolean isSelected = checkBox.isSelected();
		return isSelected;
	}

}

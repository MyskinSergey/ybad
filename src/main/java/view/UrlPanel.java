package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import entity.FileData;
import processor.MainDataProcessor;
import util.YoutubeUtil;

public class UrlPanel extends JPanel {

	private static final long serialVersionUID = 1311879731012984494L;

	private final int DEFAULT_PLAYLIST_DOWNLOAD_COUNT = 10;
	private final int DEFAULT_INDEX_STEP = 500;

	private final String PROGRESS_ICON_IMAGE_NAME = "progress.gif";

	JPanel addUrlPanel;

	private int lastElementIndex = 1;

	private ConcurrentHashMap<JPanel, Integer> componentsIndexMap = new ConcurrentHashMap<JPanel, Integer>();
	private ConcurrentHashMap<JPanel, FileData> componentsDataMap = new ConcurrentHashMap<JPanel, FileData>();
	private ConcurrentHashMap<JPanel, JPanel> urlComponentsMap = new ConcurrentHashMap<JPanel, JPanel>();
	private ConcurrentHashMap<JPanel, Integer> urlComponentsCountMap = new ConcurrentHashMap<JPanel, Integer>();

	public UrlPanel() {
		super();

		setAutoscrolls(true);
		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(0, 10, 0, 10));

		addStubPanel();
	}

	void addUrlComponent() {
		JPanel urlComponent = createUrlComponent();
		addComponent(urlComponent, lastElementIndex);
	}

	private void addComponent(JPanel componentPanel, int index) {

		componentsIndexMap.put(componentPanel, index);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0; // cell number coordinate
		gbc.gridy = index;
		gbc.gridwidth = 1; // cell count to place
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL; // direction when resize
		gbc.anchor = GridBagConstraints.NORTH; // position
		gbc.weightx = 0.1;
		gbc.weighty = 0;

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				add(componentPanel, gbc); // add component to the ContentPane

				MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
				mainWindow.validate();
				mainWindow.repaint();
			}
		});

		lastElementIndex += DEFAULT_INDEX_STEP;
	}

	private int removeComponent(Component component) {
		Integer removedIndex = componentsIndexMap.remove(component);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				remove(component);

				MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
				mainWindow.validate();
				mainWindow.repaint();
			}
		});

		return removedIndex;
	}

	private JPanel createUrlComponent() {
		JPanel urlPanel = new JPanel(new BorderLayout());

		urlPanel.add(Box.createRigidArea(new Dimension(0, 10)), BorderLayout.NORTH);

		JLabel urlLabel = new JLabel("URL of Youtube video/playlist: ");
		urlPanel.add(urlLabel, BorderLayout.WEST);

		JTextField urlField = new JTextField();
		urlField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent event) {
				onUrlTextFieldChange(urlPanel);
			}

			@Override
			public void removeUpdate(DocumentEvent event) {
				onUrlTextFieldChange(urlPanel);
			}

			@Override
			public void changedUpdate(DocumentEvent event) {
				onUrlTextFieldChange(urlPanel);
			}
		});
		urlPanel.add(urlField, BorderLayout.CENTER);

		JLabel progressImageLabel = createProgressImageLabel();
		urlPanel.add(progressImageLabel, BorderLayout.EAST);

		urlPanel.add(Box.createRigidArea(new Dimension(0, 10)), BorderLayout.SOUTH);

		return urlPanel;
	}

	private void onUrlTextFieldChange(JPanel urlPanel) {
		showProgressImage(urlPanel);

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				JTextField urlField = (JTextField) urlPanel.getComponent(2);
				String url = urlField.getText();

				boolean isVideoUrl = YoutubeUtil.isVideoUrl(url);
				if (isVideoUrl) {
					MainDataProcessor mainDataProcessor = new MainDataProcessor();
					FileData fileInfo = mainDataProcessor.getFileInfo(url);

					if (fileInfo != null) {
						JPanel videoInfoComponent = new VideoInfoComponent(fileInfo);
						componentsDataMap.put(videoInfoComponent, fileInfo);

						int removedIndex = removeComponent(urlPanel);
						addComponent(videoInfoComponent, removedIndex);

					} else {
						// TODO: Show error message
					}
				} else {
					boolean isChannelUrl = YoutubeUtil.isChannelUrl(url);
					boolean isPlaylistUrl = YoutubeUtil.isPlaylistUrl(url);
					if (isChannelUrl || isPlaylistUrl) {
						processChannelUrl(urlPanel, null);
					}
				}
			}

		});
		thread.start();
	}

	private JPanel createDownloadMoreVideoButtonComponent() {
		JPanel downloadMoreVideoButtonPanel = new JPanel();
		downloadMoreVideoButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton downloadMoreVideoButton = new JButton("Загрузить еще");
		downloadMoreVideoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Thread thread = new Thread(new Runnable() {

					@Override
					public void run() {
						int lastIndex = removeComponent(downloadMoreVideoButtonPanel);

						JPanel urlPanel = urlComponentsMap.get(downloadMoreVideoButtonPanel);
						processChannelUrl(urlPanel, lastIndex);
					}
				});
				thread.start();
			}

		});

		downloadMoreVideoButtonPanel.add(downloadMoreVideoButton);

		return downloadMoreVideoButtonPanel;
	}

	private void processChannelUrl(JPanel urlPanel, Integer lastIndex) {
		JTextField urlField = (JTextField) urlPanel.getComponent(2);
		String url = urlField.getText();

		MainDataProcessor mainDataProcessor = new MainDataProcessor();
		JPanel previuosAddedComponent = null;

		Integer lastFileIndex = urlComponentsCountMap.get(urlPanel);
		Integer startFileIndex = (lastFileIndex != null) ? (lastFileIndex + 1) : 1;
		Integer endFileIndex = startFileIndex + (DEFAULT_PLAYLIST_DOWNLOAD_COUNT - 1);

		for (int fileIndex = startFileIndex; fileIndex <= endFileIndex; fileIndex++) {

			boolean isLastAddedComponent = (fileIndex == endFileIndex);
			FileData fileInfo = mainDataProcessor.getFileInfo(url, fileIndex);

			if (fileInfo != null) {
				JPanel videoInfoComponent = new VideoInfoComponent(fileInfo);
				componentsDataMap.put(videoInfoComponent, fileInfo);
				urlComponentsCountMap.put(urlPanel, fileIndex);

				if (previuosAddedComponent == null) {
					if (lastIndex == null) {
						lastIndex = componentsIndexMap.get(urlPanel);
					}
				} else {
					lastIndex = componentsIndexMap.get(previuosAddedComponent);
				}
				Integer nextIndex = lastIndex + 1;
				previuosAddedComponent = videoInfoComponent;

				addComponent(videoInfoComponent, nextIndex);

				// Add button after last component
				if (isLastAddedComponent) {
					Integer downloadMoreVideoButtonPanelIndex = nextIndex + 1;
					JPanel downloadMoreVideoButtonPanel = createDownloadMoreVideoButtonComponent();

					urlComponentsMap.put(downloadMoreVideoButtonPanel, urlPanel);
					addComponent(downloadMoreVideoButtonPanel, downloadMoreVideoButtonPanelIndex);
				}
			} else {
				// TODO: Show error message
			}
		}
	}

	private void addStubPanel() {
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = Character.MAX_VALUE;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.SOUTH;
		gbc.weightx = 1;
		gbc.weighty = 1;

		JPanel stubPanel = new JPanel();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				add(stubPanel, gbc);
			}
		});
	}

	private JLabel createProgressImageLabel() {
		URL imageURL = getClass().getResource("/images/" + PROGRESS_ICON_IMAGE_NAME);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(imageURL);

		ImageIcon imageIcon = new ImageIcon(image);
		imageIcon.setImageObserver(UrlPanel.this);

		JLabel imageIconLabel = new JLabel(imageIcon);
		imageIconLabel.setVisible(false);

		return imageIconLabel;
	}

	private void showProgressImage(JPanel urlPanel) {
		JLabel imageIconLabel = (JLabel) urlPanel.getComponent(3);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				imageIconLabel.setVisible(true);

				MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
				mainWindow.validate();
				mainWindow.repaint();
			}
		});
	}

	public Collection<FileData> getFileDataToDownload() {
		List<FileData> dataToDownloadList = new LinkedList<FileData>();
		for (JPanel videoInfoPanel : componentsDataMap.keySet()) {
			boolean isSelected = ((VideoInfoComponent) videoInfoPanel).isSelected();
			if (isSelected) {
				FileData fileData = componentsDataMap.get(videoInfoPanel);
				dataToDownloadList.add(fileData);
			}
		}
		return dataToDownloadList;
	}

}

package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.net.URL;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import model.label.Label;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = 3172688540921699213L;

	private final String waightIconImageName = "wait-icon.gif";

	private final int DEFAULT_WIDTH = 800;
	private final int DEFAULT_HEIGHT = 500;

	private final int MINIMUM_WIDTH = 400;
	private final int MINIMUN_HEIGHT = 200;

	private UrlPanel urlPanel;
	private JPanel logPanel;
	private JScrollPane logScrollPane;

	public MainWindow() {
		super();
		createView();
	}

	private void createView() {
		setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();

		TopPanel topPanel = new TopPanel();

		gbc.gridx = 0; // cell number coordinate
		gbc.gridy = 0;
		gbc.gridwidth = 1; // cell count to place
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL; // direction when resize
		gbc.anchor = GridBagConstraints.NORTH; // position
		gbc.weightx = 0.1;
		gbc.weighty = 0;
		add(topPanel, gbc); // add component to the ContentPane

		JScrollPane urlScrollPane = new JScrollPane();
		urlPanel = new UrlPanel();
		urlScrollPane.setViewportView(urlPanel);

		Border urlPaneBorder = BorderFactory.createEtchedBorder();
		urlScrollPane.setBorder(BorderFactory.createTitledBorder(urlPaneBorder, "URL's"));

		logPanel = new JPanel();
		logPanel.setAutoscrolls(true);
		logPanel.setLayout(new BoxLayout(logPanel, BoxLayout.Y_AXIS));

		logScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		logScrollPane.setViewportView(logPanel);
		logScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
			}
		});
		Border logPaneBorder = BorderFactory.createEtchedBorder();
		logScrollPane.setBorder(BorderFactory.createTitledBorder(logPaneBorder, "Log"));

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, urlScrollPane, logScrollPane);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(DEFAULT_HEIGHT / 2);

		gbc.gridx = 0; // cell number coordinate
		gbc.gridy = 1;
		gbc.gridwidth = 1; // cell count to place
		gbc.gridheight = 1;
		gbc.fill = GridBagConstraints.BOTH; // direction when resize
		gbc.anchor = GridBagConstraints.NORTH; // position
		gbc.weightx = 1;
		gbc.weighty = 1;
		add(splitPane, gbc); // add component to the ContentPane

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setMinimumSize(new Dimension(MINIMUM_WIDTH, MINIMUN_HEIGHT));

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		setBounds((screenSize.width - getWidth()) / 2, (screenSize.height - getHeight()) / 2, getWidth(), getHeight());
		setTitle(Label.YOUTUBE_BEST_AUDIO_DOWNLOADER);
	}

	void updateTextPanel(List<JLabel> labelsList) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				setTextData(labelsList);

				validate();
				repaint();
			}
		});
	}

	private void setTextData(List<JLabel> labelsList) {
		logScrollPane.setViewportView(logPanel);
		logPanel.removeAll();
		logPanel.add(new JLabel(" "));
		for (JLabel label : labelsList) {
			logPanel.add(label);
		}
	}

	public void addTextData(String text) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				logScrollPane.setViewportView(logPanel);
				JLabel label = new JLabel("   " + text);
				logPanel.add(label);
			}
		});
	}

	// TODO: Move this in another place (top panel for example)
	private void showWaitImage(boolean visible) {
		logPanel.removeAll();

		URL imageURL = getClass().getResource("/images/" + waightIconImageName);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(imageURL);

		ImageIcon imageIcon = new ImageIcon(image);
		imageIcon.setImageObserver(MainWindow.this);

		logPanel.add(new JLabel(imageIcon));
		logPanel.add(new JLabel(" "));
		logPanel.add(new JLabel(Label.WAIT_FOR_HANDLING));

		validate();
		repaint();
	}

	public UrlPanel getUrlPanel() {
		return urlPanel;
	}

}

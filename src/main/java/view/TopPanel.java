package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import entity.FileData;
import model.label.Label;
import processor.MainDataProcessor;
import util.Logger;

public class TopPanel extends JPanel {

	private final int DEFAULT_HEIGHT = 70;

	private static final long serialVersionUID = 2699867398337249080L;

	private JButton startButton;

	private String downloadPath;

	public TopPanel() {
		super();

		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(0, DEFAULT_HEIGHT));

		Border topPanelBorder = BorderFactory.createEtchedBorder();
		setBorder(BorderFactory.createTitledBorder(topPanelBorder, "Actions"));

		add(createAddUrlFieldButton(), BorderLayout.LINE_START);
		add(createDownloadFolderChooserField(), BorderLayout.CENTER);
		add(createStartFieldButton(), BorderLayout.LINE_END);
	}

	private JPanel createStartFieldButton() {
		JPanel startButtonPanel = new JPanel();

		// TODO: Add check if no video selected - make button disabled
		startButton = new JButton(Label.START);
		startButton.setEnabled(false);
		startButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// showWaitImage(true);
				// TODO: Fixed issue when try to download second time the same
				// selected file
				Thread thread = new Thread(new Runnable() {

					public void run() {
						try {
							MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
							UrlPanel urlPanel = mainWindow.getUrlPanel();
							Collection<FileData> fileDataToDownload = urlPanel.getFileDataToDownload();

							MainDataProcessor mainDataProcessor = new MainDataProcessor();
							mainDataProcessor.downloadBestAudio(fileDataToDownload, downloadPath);

							List<JLabel> labelsList = new ArrayList<JLabel>(1);
							JLabel label = new JLabel("   " + Label.DOWNLOADING_IS_FINISHED);
							labelsList.add(label);

							mainWindow.updateTextPanel(labelsList);
						} catch (Exception ex) {
							ex.printStackTrace();
							Logger.error(ex);
						}
					}
				});
				thread.start();
			}

		});
		startButtonPanel.add(startButton);
		return startButtonPanel;
	}

	private JPanel createDownloadFolderChooserField() {
		JPanel downloadFolderChooserPanel = new JPanel();

		JTextField downloadPathTextField = new JTextField();
		downloadPathTextField.setEnabled(false);
		downloadPathTextField.setVisible(false);

		JButton choosePathButton = new JButton(Label.CHOOSE_PATH);
		choosePathButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				final JFileChooser donwloadFolderChooser = new JFileChooser();
				donwloadFolderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				int returnVal = donwloadFolderChooser.showOpenDialog(getParent());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File donwloadFolder = donwloadFolderChooser.getSelectedFile();
					String donwloadFolderAbsolutePath = donwloadFolder.getAbsolutePath();

					downloadPathTextField.setText(" " + donwloadFolderAbsolutePath + " ");
					downloadPathTextField.setVisible(true);
					Logger.info("Download path was set to: " + donwloadFolderAbsolutePath);
					setDownloadPath(donwloadFolderAbsolutePath);

					startButton.setEnabled(true);

					MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
					mainWindow.validate();
					mainWindow.repaint();
				}
			}

		});
		downloadFolderChooserPanel.add(downloadPathTextField);
		downloadFolderChooserPanel.add(choosePathButton);

		return downloadFolderChooserPanel;
	}

	private JPanel createAddUrlFieldButton() {
		JPanel addUrlButtonPanel = new JPanel();
		JButton addUrlButton = new JButton("Добавить URL");

		addUrlButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainWindow mainWindow = (MainWindow) getTopLevelAncestor();
				UrlPanel urlPanel = mainWindow.getUrlPanel();
				urlPanel.addUrlComponent();
			}
		});

		addUrlButtonPanel.add(addUrlButton);
		return addUrlButtonPanel;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	private void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

}

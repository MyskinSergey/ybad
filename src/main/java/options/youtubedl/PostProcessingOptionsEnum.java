package options.youtubedl;

public enum PostProcessingOptionsEnum {

	FFMPEG_LOCATION("--ffmpeg-location",
			"Location of the ffmpeg/avconv binary; either the path to the binary or its containing directory."), 
	EXTRACT_AUDIO("--extract-audio",
					"Convert video files to audio-only files (requires ffmpeg or avconv and ffprobe or avprobe)");

	PostProcessingOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

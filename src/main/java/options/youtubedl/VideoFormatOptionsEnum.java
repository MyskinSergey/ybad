package options.youtubedl;

public enum VideoFormatOptionsEnum {

	FORMAT("--format", "Video format code, see the \"FORMAT SELECTION\" for all the info"),
	LIST_FORMATS("--list-formats", "List all available formats of requested videos");

	VideoFormatOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

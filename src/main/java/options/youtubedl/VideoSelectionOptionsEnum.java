package options.youtubedl;

public enum VideoSelectionOptionsEnum {

	PLAYLIST_START("--playlist-start", "Playlist video to start at (default is 1)"), 
	PLAYLIST_END("--playlist-end", "Playlist video to end at (default is last)"), 
	PLAYLIST_ITEMS("--playlist-items", "Playlist video items to download. Specify indices of the videos in the playlist separated by commas like: \"--playlist-items 1,2,5,8\" if you want to download videos indexed 1, 2, 5, 8 in the playlist. You can specify range: \"--playlist-items 1-3,7,10-13\", it will download the videos at index 1, 2, 3, 7, 10, 11, 12 and 13.");

	VideoSelectionOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

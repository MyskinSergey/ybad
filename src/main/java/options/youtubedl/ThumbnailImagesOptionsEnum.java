package options.youtubedl;

// TODO: Delete if won't be using 
public enum ThumbnailImagesOptionsEnum {
	
	WRITE_THUMBNAIL("--write-thumbnail", "Write thumbnail image to disk"),
	WRITE_ALL_THUMBNAILS("--write-all-thumbnails", "Write all thumbnail image formats to disk"),
	LIST_THUMBNAILS("--list-thumbnails", "Simulate and list all available thumbnail formats");
	
	ThumbnailImagesOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

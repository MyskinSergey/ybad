package options.youtubedl;

public enum DownloadOptionsEnum {

	RETRIES("--retries", "Number of retries (default is 10), or \"infinite\"."), 
	FRAGMENT_RETRIES("--fragment-retries",
			"Number of retries for a fragment (default is 10), or \"infinite\" (DASH only)");

	DownloadOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

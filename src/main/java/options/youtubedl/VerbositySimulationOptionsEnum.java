package options.youtubedl;

public enum VerbositySimulationOptionsEnum {
	
	SIMULATE("--simulate", "Do not download the video and do not write anything to disk"),
	GET_URL("--get-url", "Simulate, quiet but print URL"),
	GET_TITLE("--get-title", "Simulate, quiet but print title"),
	GET_ID("--get-id", "Simulate, quiet but print id"),
	GET_THUMBNAIL("--get-thumbnail", "Simulate, quiet but print thumbnail URL"),
	GET_DESCRIPTION("--get-description", "Simulate, quiet but print video description"),
	GET_DURATION("--get-duration", "Simulate, quiet but print video length"),
	GET_FILENAME("--get-filename", "Simulate, quiet but print output filename"),
	GET_FORMAT("--get-format", "Simulate, quiet but print output format"),
	PRINT_JSON("--print-json", "Be quiet and print the video information as JSON (video is still being downloaded).");
	
	VerbositySimulationOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

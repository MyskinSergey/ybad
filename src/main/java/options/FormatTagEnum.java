package options;

public enum FormatTagEnum {
	
	DASH_AUDIO("DASH audio"), 
	DASH_VIDEO("DASH video"); 
	
	private FormatTagEnum(String tag) {
		this.tag = tag;
	}
	
	private String tag;

	public String getTag() {
		return tag;
	}

}

package options.ffmpeg;

/**
 * Color enum for FFMpeg utility. For more colors use command "ffmpeg -colors"
 * 
 * @author LowApmMaster
 *
 */
public enum FFMpegColorEnum {

	BLACK("Black", "#000000"), 
	DARKGRAY("DarkGray", "#a9a9a9"), 
	GRAY("Gray", "#808080"), 
	WHITE("White", "#ffffff"), 
	WHITESMOKE("WhiteSmoke", "#f5f5f5");

	FFMpegColorEnum(String name, String htmlCode) {
		this.name = name;
		this.htmlCode = htmlCode;
	}

	private String name;
	private String htmlCode;

	public String getName() {
		return name;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

}

package options.ffmpeg;

public enum FFMpegVideoOptionsEnum {

	ASPECT("-aspect",
			"[:stream_specifier] aspect (output,per-stream) Set the video display aspect ratio specified by aspect. aspect can be a floating point number string, or a string of the form num:den, where num and den are the numerator and denominator of the aspect ratio. For example \"4:3\", \"16:9\", \"1.3333\", and \"1.7777\" are valid argument values. If used together with -vcodec copy, it will affect the aspect ratio stored at container level, but not the aspect ratio stored in encoded frames, if it exists."), 
	FILTERGRAPH("-vf",
			"filtergraph (output) Create the filtergraph specified by filtergraph and use it to filter the stream. This is an alias for -filter:v, see the -filter option.");

	FFMpegVideoOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

package options.ffmpeg;

public enum FFMpegFilterOptionsEnum {

	SCALE("scale",
			"Scale (resize) the input video, using the libswscale library. The scale filter forces the output display aspect ratio to be the same of the input, by changing the output sample aspect ratio. If the input image format is different from the format requested by the next filter, the scale filter will convert the input to the requested format."), 
	PAD("pad", 
			"Add paddings to the input image, and place the original input at the provided x, y coordinates.");

	FFMpegFilterOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

package options.ffmpeg;

public enum FFMpegMainOptionsEnum {
	
	I("-i", "filename (input)"),
    Y("-y", "input file name (global) Overwrite output files without asking.");
    
    FFMpegMainOptionsEnum(String option, String description) {
		this.option = option;
		this.description = description;
	}

	private String option;
	private String description;

	public String getOption() {
		return option;
	}

	public String getDescription() {
		return description;
	}

}

package options;

public enum AudioYoutubeExtensionEnum {

	M4A, 
	OPUS, 
	OGG;

	public static boolean isYoutubeAudioExtension(String extension) {
		AudioYoutubeExtensionEnum[] values = AudioYoutubeExtensionEnum.values();
		for (AudioYoutubeExtensionEnum audioYoutubeExtension : values) {
			String name = audioYoutubeExtension.name();
			if (name.equalsIgnoreCase(extension)) {
				return true;
			}
		}
		return false;
	}
}

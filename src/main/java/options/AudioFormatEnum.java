package options;

public enum AudioFormatEnum {
	
	AAC(2),
	OPUS(3),
	OGG(1);
	
	private Integer qualityPriority;

	private AudioFormatEnum(Integer qualityPriority) {
		this.qualityPriority = qualityPriority;
	}

	public Integer getQualityPriority() {
		return qualityPriority;
	}
	
}

package options.kid3;

/**
 * Kid3 some commands list.
 * 
 * If you need more commands, see
 * <a href="http://kid3.sourceforge.net/kid3_en.html#kid3-cli-commands">Kid3
 * Commands</a>
 * 
 * @author LowApmMaster
 *
 */
public enum Kid3CommansEnum {

	SET_TAG_FRAME("set",
			"set {FRAME-NAME} {FRAME-VALUE} [TAG-NUMBERS] This command sets the value of a specific tag frame. ");

	Kid3CommansEnum(String command, String description) {
		this.command = command;
		this.description = description;
	}

	private String command;
	private String description;

	public String getCommand() {
		return command;
	}

	public String getDescription() {
		return description;
	}

}

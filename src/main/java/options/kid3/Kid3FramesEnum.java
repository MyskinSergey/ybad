package options.kid3;

/**
 * Kid3 some frames list.
 * 
 * If you need more frames, see
 * <a href="http://kid3.sourceforge.net/kid3_en.html#frame-list">Kid3 Frame
 * List</a>
 * 
 * @author LowApmMaster
 *
 */
public enum Kid3FramesEnum {

	TITLE("title"), 
	ARTIST("artist"),
	ALBUM("album"),
	COMMENT("comment"),
	DATE("date"),
	PICTURE("picture");
	
	private Kid3FramesEnum(String frameName) {
		this.frameName = frameName;
	}

	private String frameName;

	public String getFrameName() {
		return frameName;
	}
	
}

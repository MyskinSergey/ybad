package options;

public enum AudioContainerEnum {
	
	M4A(AudioFormatEnum.AAC, AudioYoutubeExtensionEnum.M4A),
	MP4A(AudioFormatEnum.AAC, AudioYoutubeExtensionEnum.M4A),
	OPUS(AudioFormatEnum.OPUS, AudioYoutubeExtensionEnum.OPUS),
	OGG(AudioFormatEnum.OGG, AudioYoutubeExtensionEnum.OGG),
	VORBIS(AudioFormatEnum.OGG, AudioYoutubeExtensionEnum.OGG);
	
	private AudioFormatEnum audioFormat;
	private AudioYoutubeExtensionEnum audioExtension;
	
	private AudioContainerEnum(AudioFormatEnum audioFormat, AudioYoutubeExtensionEnum audioExtension) {
		this.audioFormat = audioFormat;
		this.audioExtension = audioExtension;
	}
	
	public AudioFormatEnum getAudioFormat() {
		return audioFormat;
	}
	
	public void setAudioFormat(AudioFormatEnum audioFormat) {
		this.audioFormat = audioFormat;
	}
	
	public AudioYoutubeExtensionEnum getAudioExtension() {
		return audioExtension;
	}
	
	public void setAudioExtension(AudioYoutubeExtensionEnum audioExtension) {
		this.audioExtension = audioExtension;
	}
		
}

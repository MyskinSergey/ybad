package options;

public enum LatinEncodingCaseEnum {
	
	DOWNLOAD_PATH_ONLY,
	FILE_NAME_ONLY,
	DOWNLOAD_PATH_AND_FILE_NAME,
	NO_ITEMS;

}

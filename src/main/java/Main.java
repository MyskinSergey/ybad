import javax.swing.SwingUtilities;

import model.label.Label;
import util.Logger;
import view.MainWindow;

public class Main {

	public static void main(String[] args) {
		Label label = new Label();
		label.init();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				MainWindow mainWindow = new MainWindow();

				Logger logger = new Logger();
				logger.setMainWindow(mainWindow);
			}
		});

	}

}

package entity;

import options.AudioFormatEnum;

public class AudioQuality {

	private Integer formatCode;
	private String videoExtension;
	private AudioFormatEnum audioFormat;
	private Integer bitrate;
	private Long size;
	private boolean isAudioOnlyFormat;
	private Integer downloadIndex;

	public Integer getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Integer formatCode) {
		this.formatCode = formatCode;
	}

	public String getVideoExtension() {
		return videoExtension;
	}

	public void setVideoExtension(String videoExtension) {
		this.videoExtension = videoExtension;
	}

	public AudioFormatEnum getAudioFormat() {
		return audioFormat;
	}

	public void setAudioFormat(AudioFormatEnum audioFormat) {
		this.audioFormat = audioFormat;
	}

	public Integer getBitrate() {
		return bitrate;
	}

	public void setBitrate(Integer bitrate) {
		this.bitrate = bitrate;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public boolean isAudioOnlyFormat() {
		return isAudioOnlyFormat;
	}

	public void setAudioOnlyFormat(boolean isAudioOnlyFormat) {
		this.isAudioOnlyFormat = isAudioOnlyFormat;
	}

	public Integer getDownloadIndex() {
		return downloadIndex;
	}

	public void setDownloadIndex(Integer downloadIndex) {
		this.downloadIndex = downloadIndex;
	}

	public boolean isSameAudioQuality(AudioQuality audioQuality) {
		if (audioQuality == null) {
			return false;
		}
		if (this.isAudioOnlyFormat != audioQuality.isAudioOnlyFormat()) {
			return false;
		}
		if (!this.audioFormat.equals(audioQuality.getAudioFormat())) {
			return false;
		}
		if ((this.bitrate == null) && (audioQuality.getBitrate() == null)) {
			return true;
		}
		if ((this.bitrate != null) && (audioQuality.getBitrate() == null)) {
			return false;
		}
		if ((this.bitrate == null) && (audioQuality.getBitrate() != null)) {
			return false;
		}
		if (!this.bitrate.equals(audioQuality.getBitrate())) {
			return false;
		}
		return true;
	}

}

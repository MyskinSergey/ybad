package entity;

import java.io.File;
import java.util.Date;
import java.util.List;

import options.LatinEncodingCaseEnum;

public class FileData {

	private Integer index;
	private String title;
	private String videoUrl;
	private String duration;
	private Date uploadDate;

	private String thumbnailImageUrl;
	private Integer thumbnailImageWidth;
	private Integer thumbnailImageHeight;

	private String playlistUrl;
	private String playlistName;

	private String channelName;
	private String channelUrl;

	private List<AudioQuality> audioQualityList;

	private String downloadAudioFolderPath;
	private String downloadAudioFileName;
	private LatinEncodingCaseEnum latinEncodingCase;

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getThumbnailImageUrl() {
		return thumbnailImageUrl;
	}

	public void setThumbnailImageUrl(String thumbnailImageUrl) {
		this.thumbnailImageUrl = thumbnailImageUrl;
	}

	public String getPlaylistName() {
		return playlistName;
	}

	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	public List<AudioQuality> getAudioQualityList() {
		return audioQualityList;
	}

	public void setAudioQualityList(List<AudioQuality> audioQualityList) {
		this.audioQualityList = audioQualityList;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getPlaylistUrl() {
		return playlistUrl;
	}

	public void setPlaylistUrl(String playlistUrl) {
		this.playlistUrl = playlistUrl;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelUrl() {
		return channelUrl;
	}

	public void setChannelUrl(String channelUrl) {
		this.channelUrl = channelUrl;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Integer getThumbnailImageWidth() {
		return thumbnailImageWidth;
	}

	public void setThumbnailImageWidth(Integer thumbnailImageWidth) {
		this.thumbnailImageWidth = thumbnailImageWidth;
	}

	public Integer getThumbnailImageHeight() {
		return thumbnailImageHeight;
	}

	public void setThumbnailImageHeight(Integer thumbnailImageHeight) {
		this.thumbnailImageHeight = thumbnailImageHeight;
	}

	public String getDownloadAudioFolderPath() {
		return downloadAudioFolderPath;
	}

	public void setDownloadAudioFolderPath(String downloadAudioFolderPath) {
		this.downloadAudioFolderPath = downloadAudioFolderPath;
	}

	public String getDownloadAudioFileName() {
		return downloadAudioFileName;
	}

	public void setDownloadAudioFileName(String downloadAudioFileName) {
		this.downloadAudioFileName = downloadAudioFileName;
	}

	public String getDownloadAudioFullPath() {
		String downloadAudioFullPath = this.downloadAudioFolderPath + File.separator + this.downloadAudioFileName;
		return downloadAudioFullPath;
	}

	public LatinEncodingCaseEnum getLatinEncodingCase() {
		return latinEncodingCase;
	}

	public void setLatinEncodingCase(LatinEncodingCaseEnum latinEncodingCase) {
		this.latinEncodingCase = latinEncodingCase;
	}

}

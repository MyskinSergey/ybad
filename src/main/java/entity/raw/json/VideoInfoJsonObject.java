package entity.raw.json;

import java.util.List;

public class VideoInfoJsonObject {

	private String fulltitle; // video title (Making of Tubus Show)
	private String format_id; // video format id (22)
	private String id; // video id (_D3b4X6HKGs)
	private String ext; // extention (mp4)
	private String thumbnail; // thumbnail image URL 
								// (https://i.ytimg.com/vi/_D3b4X6HKGs/hqdefault.jpg)
	private String webpage_url; // (https://www.youtube.com/watch?v=_D3b4X6HKGs)
	private String uploader_id; // (tubushow)

	private List<FormatJsonObject> formats;

	private String display_id; // (_D3b4X6HKGs)
	private String title; // (Making ofTubus Show)
	private Integer duration; // (75)
	private String playlist; // (null)
	private String uploader; // (Tubus Show)
	private String upload_date; // (20160107)
	private String uploader_url; // (http://www.youtube.com/user/tubushow)
	private String playlist_index; // (null)

	public String getFulltitle() {
		return fulltitle;
	}

	public void setFulltitle(String fulltitle) {
		this.fulltitle = fulltitle;
	}

	public String getFormat_id() {
		return format_id;
	}

	public void setFormat_id(String format_id) {
		this.format_id = format_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getWebpage_url() {
		return webpage_url;
	}

	public void setWebpage_url(String webpage_url) {
		this.webpage_url = webpage_url;
	}

	public String getUploader_id() {
		return uploader_id;
	}

	public void setUploader_id(String uploader_id) {
		this.uploader_id = uploader_id;
	}

	public List<FormatJsonObject> getFormats() {
		return formats;
	}

	public void setFormats(List<FormatJsonObject> formats) {
		this.formats = formats;
	}

	public String getDisplay_id() {
		return display_id;
	}

	public void setDisplay_id(String display_id) {
		this.display_id = display_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getPlaylist() {
		return playlist;
	}

	public void setPlaylist(String playlist) {
		this.playlist = playlist;
	}

	public String getUploader() {
		return uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	public String getUpload_date() {
		return upload_date;
	}

	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}

	public String getUploader_url() {
		return uploader_url;
	}

	public void setUploader_url(String uploader_url) {
		this.uploader_url = uploader_url;
	}

	public String getPlaylist_index() {
		return playlist_index;
	}

	public void setPlaylist_index(String playlist_index) {
		this.playlist_index = playlist_index;
	}

}

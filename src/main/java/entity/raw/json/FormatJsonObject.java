package entity.raw.json;

public class FormatJsonObject {

	private String preference; // =-50,
	private String width; // =null,
	private String protocol; // =https,
	private String format; // =249- audio only (DASH audio),
	private String ext; // =webm,
	private Long filesize; // =475054,
	private String format_note; // =DASH audio,
	private Integer abr; // =50,
	private Integer tbr; // =56,
	private String acodec; // =opus,
	private Integer asr; // =48000,
	private Integer format_id; // =249

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public Long getFilesize() {
		return filesize;
	}

	public void setFilesize(Long filesize) {
		this.filesize = filesize;
	}

	public String getFormat_note() {
		return format_note;
	}

	public void setFormat_note(String format_note) {
		this.format_note = format_note;
	}

	public Integer getAbr() {
		return abr;
	}

	public void setAbr(Integer abr) {
		this.abr = abr;
	}

	public Integer getTbr() {
		return tbr;
	}

	public void setTbr(Integer tbr) {
		this.tbr = tbr;
	}

	public String getAcodec() {
		return acodec;
	}

	public void setAcodec(String acodec) {
		this.acodec = acodec;
	}

	public Integer getAsr() {
		return asr;
	}

	public void setAsr(Integer asr) {
		this.asr = asr;
	}

	public Integer getFormat_id() {
		return format_id;
	}

	public void setFormat_id(Integer format_id) {
		this.format_id = format_id;
	}

}

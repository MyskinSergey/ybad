package config;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import util.Logger;

public class ConfigUtils {

	public static final int FAKE_ID = -1;
	public static final String DEFAULT_ENCODING = "UTF-8";

	public static final String PROJECT_FOLDER_PATH;

	public static final String YOUTUBE_DL_PATH;
	public static final String FFMPEG_PATH;
	public static final String FFBROBE_PATH;
	public static final String KID3_PATH;

	static {
		PROJECT_FOLDER_PATH = getProjectFolderPath();

		YOUTUBE_DL_PATH = getAbsolutePath(BinUtilsEnum.YOUTUBE_DL_FILE_NAME);
		FFMPEG_PATH = getAbsolutePath(BinUtilsEnum.FFMPEG_FILE_NAME);
		FFBROBE_PATH = getAbsolutePath(BinUtilsEnum.FFBROBE_FILE_NAME);
		KID3_PATH = getAbsolutePath(BinUtilsEnum.KID3_FILE_NAME);
	}

	private static String getAbsolutePath(BinUtilsEnum binUtil) {
		String ulilityFileName = binUtil.getUtilFileName();
		String utilRelativeFolderPath = binUtil.getUtilRelativeFolderPath();
		String utilPath = PROJECT_FOLDER_PATH + utilRelativeFolderPath + ulilityFileName;

		File utilFile = new File(utilPath);
		String ulilityFileAbsolutePath = utilFile.getAbsolutePath();

		return ulilityFileAbsolutePath;
	}

	public static String getProjectFolderPath() {
		if (PROJECT_FOLDER_PATH == null) {
			try {
				String exeFilePath = ConfigUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath();
				String decodedExeFilePath = URLDecoder.decode(exeFilePath, DEFAULT_ENCODING);
				File exeFile = new File(decodedExeFilePath);

				String projectFolderPath = exeFile.getParent();
				Logger.info("Project path: " + projectFolderPath);
				return projectFolderPath;
			} catch (UnsupportedEncodingException e) {
				Logger.error(e);
			}
		} else {
			return PROJECT_FOLDER_PATH;
		}
		return null;
	}

}

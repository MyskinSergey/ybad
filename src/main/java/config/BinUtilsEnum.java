package config;

public enum BinUtilsEnum {

	YOUTUBE_DL_FILE_NAME("youtube-dl.exe", "\\bin\\youtube-dl\\32\\"),
	FFMPEG_FILE_NAME("ffmpeg.exe", "\\bin\\ffmpeg\\64\\"), 
	FFBROBE_FILE_NAME("ffprobe.exe", "\\bin\\ffmpeg\\64\\"), 
	KID3_FILE_NAME("kid3-cli.exe", "\\bin\\kid3\\32\\");

	private BinUtilsEnum(String utilFileName, String utilRelativeFolderPath) {
		this.utilFileName = utilFileName;
		this.utilRelativeFolderPath = utilRelativeFolderPath;
	}
	
	private String utilFileName;
	private String utilRelativeFolderPath;
	
	public String getUtilFileName() {
		return utilFileName;
	}
	
	public String getUtilRelativeFolderPath() {
		return utilRelativeFolderPath;
	}

}

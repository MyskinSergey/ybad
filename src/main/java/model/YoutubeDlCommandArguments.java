package model;

import config.ConfigUtils;

public class YoutubeDlCommandArguments {

	private String url;
	private int startIndex = ConfigUtils.FAKE_ID;
	private int endIndex = ConfigUtils.FAKE_ID;;
	private int formatCode;
	private Integer downloadIndex;
	private String downloadPath;
	private String fileNameTemplate;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public int getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(int formatCode) {
		this.formatCode = formatCode;
	}

	public Integer getDownloadIndex() {
		return downloadIndex;
	}

	public void setDownloadIndex(Integer downloadIndex) {
		this.downloadIndex = downloadIndex;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getFileNameTemplate() {
		return fileNameTemplate;
	}

	public void setFileNameTemplate(String fileNameTemplate) {
		this.fileNameTemplate = fileNameTemplate;
	}

}

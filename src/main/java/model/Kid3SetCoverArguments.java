package model;

public class Kid3SetCoverArguments {

	private String sourceImagePath;
	private String sourceAudioPath;

	public String getSourceImagePath() {
		return sourceImagePath;
	}

	public void setSourceImagePath(String sourceImagePath) {
		this.sourceImagePath = sourceImagePath;
	}

	public String getSourceAudioPath() {
		return sourceAudioPath;
	}

	public void setSourceAudioPath(String sourceAudioPath) {
		this.sourceAudioPath = sourceAudioPath;
	}

}

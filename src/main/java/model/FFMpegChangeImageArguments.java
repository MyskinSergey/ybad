package model;

public class FFMpegChangeImageArguments {

	private int scaleWidth;
	private int scaleHeight;

	private int padWidth;
	private int padHeight;

	private int padX;
	private int padY;
	private String padColor;

	private String sourceUrl;
	private String destinationCoverFilePath;
	private String destinationCoverFileFolderPath;

	public int getScaleWidth() {
		return scaleWidth;
	}

	public void setScaleWidth(int scaleWidth) {
		this.scaleWidth = scaleWidth;
	}

	public int getScaleHeight() {
		return scaleHeight;
	}

	public void setScaleHeight(int scaleHeight) {
		this.scaleHeight = scaleHeight;
	}

	public int getPadWidth() {
		return padWidth;
	}

	public void setPadWidth(int padWidth) {
		this.padWidth = padWidth;
	}

	public int getPadHeight() {
		return padHeight;
	}

	public void setPadHeight(int padHeight) {
		this.padHeight = padHeight;
	}

	public int getPadX() {
		return padX;
	}

	public void setPadX(int padX) {
		this.padX = padX;
	}

	public int getPadY() {
		return padY;
	}

	public void setPadY(int padY) {
		this.padY = padY;
	}

	public String getPadColor() {
		return padColor;
	}

	public void setPadColor(String padColor) {
		this.padColor = padColor;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getDestinationCoverFilePath() {
		return destinationCoverFilePath;
	}

	public void setDestinationCoverFilePath(String destinationCoverFilePath) {
		this.destinationCoverFilePath = destinationCoverFilePath;
	}

	public String getDestinationCoverFileFolderPath() {
		return destinationCoverFileFolderPath;
	}

	public void setDestinationCoverFileFolderPath(String destinationCoverFileFolderPath) {
		this.destinationCoverFileFolderPath = destinationCoverFileFolderPath;
	}

}

package model.label;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Label {

	private static final String PATH_TO_CONFIG = "/messages.properties";

	public static String FILE_NOT_FOUND_ERROR = "";
	public static String ERROR_FILE_PROCESSING = "";
	public static String TEXT_SEPARATOR = "";
	public static String NON_WORD_CHARACTER = "";
	public static String WHITESPACE = "";
	public static String TABULATION = "";
	public static String COMA = "";
	public static String COMA_PLUS_WHITESPACE = "";
	public static String SEMICOLON = "";
	public static String CASE_SENSITIVE = "";
	public static String ENTER_OPTIONS_AND_SELECT_THE_FILE_TO_CHECK = "";
	public static String CHOOSE_PATH = "";
	public static String WAIT_FOR_HANDLING = "";
	public static String RUS_REGEXP_1 = "";
	public static String RUS_REGEXP_2 = "";
	public static String TERM = "";
	public static String THE_NUMBER_OF_FOUND = "";
	
	public static String START = "";
	public static String YOUTUBE_BEST_AUDIO_DOWNLOADER = "";
	public static String DOWNLOADING_IS_FINISHED = "";

	public void init() {
		try {
			InputStream resourceAsStream = getClass().getResourceAsStream(PATH_TO_CONFIG);
			Properties properties = PropertyLoader.load(resourceAsStream);
			properties.load(resourceAsStream);

			FILE_NOT_FOUND_ERROR = properties.getProperty("FILE_NOT_FOUND_ERROR");
			ERROR_FILE_PROCESSING = properties.getProperty("ERROR_FILE_PROCESSING");
			TEXT_SEPARATOR = properties.getProperty("TEXT_SEPARATOR");
			NON_WORD_CHARACTER = properties.getProperty("NON_WORD_CHARACTER");
			WHITESPACE = properties.getProperty("WHITESPACE");
			TABULATION = properties.getProperty("TABULATION");
			COMA = properties.getProperty("COMA");
			COMA_PLUS_WHITESPACE = properties.getProperty("COMA_PLUS_WHITESPACE");
			SEMICOLON = properties.getProperty("SEMICOLON");
			CASE_SENSITIVE = properties.getProperty("CASE_SENSITIVE");
			ENTER_OPTIONS_AND_SELECT_THE_FILE_TO_CHECK = properties.getProperty("ENTER_OPTIONS_AND_SELECT_THE_FILE_TO_CHECK");
			CHOOSE_PATH = properties.getProperty("CHOOSE_PATH");
			WAIT_FOR_HANDLING = properties.getProperty("WAIT_FOR_HANDLING");
			RUS_REGEXP_1 = properties.getProperty("RUS_REGEXP_1");
			RUS_REGEXP_2 = properties.getProperty("RUS_REGEXP_2");
			TERM = properties.getProperty("TERM");
			THE_NUMBER_OF_FOUND = properties.getProperty("THE_NUMBER_OF_FOUND");
			
			START = properties.getProperty("START");
			YOUTUBE_BEST_AUDIO_DOWNLOADER = properties.getProperty("YOUTUBE_BEST_AUDIO_DOWNLOADER");
			DOWNLOADING_IS_FINISHED = properties.getProperty("DOWNLOADING_IS_FINISHED");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
}

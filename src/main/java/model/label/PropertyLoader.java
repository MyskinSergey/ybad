package model.label;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class PropertyLoader {

	// private static final String DEFAULT_ENCODING = "Cp1251";
	private static final String DEFAULT_ENCODING = "UTF-8";

	public static Properties load(InputStream inputStream) throws IOException {
		Properties properties = new Properties();
		String buff;

		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader br = new BufferedReader(inputStreamReader);
		while ((buff = br.readLine()) != null) {
			if (parseProperty(buff) != null) {
				properties.put(parseProperty(buff)[0], parseProperty(buff)[1]);
			}

		}
		return properties;
	}

	public static Properties load(String fileName) throws IOException {
		Properties properties = new Properties();
		String buff;
		FileReader fileReader = new FileReader(fileName);
		BufferedReader br = new BufferedReader(fileReader);
		while ((buff = br.readLine()) != null) {
			if (parseProperty(buff) != null) {
				properties.put(parseProperty(buff)[0], parseProperty(buff)[1]);
			}

		}
		return properties;
	}

	private static String[] parseProperty(String propertyString) throws UnsupportedEncodingException {
		if (propertyString.contains("=")) {
			String[] prop = propertyString.split("=", 2);
			String[] result = new String[2];
			result[0] = new String(prop[0].getBytes(), DEFAULT_ENCODING);
			result[1] = new String(prop[1].getBytes(), DEFAULT_ENCODING);
			return result;
		} else {
			return null;
		}
	}

	public static String encode(String string) {
		String ENCODING = "Cp1251";
		try {
			string = new String(string.getBytes(), ENCODING);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return string;
	}

}

package command;

public enum CommandTypeEnum {
	
	DOWNLOAD_AUDIO("Download audio"),
	GET_AUDIO_INFO("Get audio info"),
	GET_VIDEO_JSON_INFO("Get video JSON info"),
	CREATE_COVER_IMAGE("Create cover image"),
	SET_COVER_IMAGE("Set cover image");

	private String description;

	private CommandTypeEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
}

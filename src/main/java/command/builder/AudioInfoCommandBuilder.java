package command.builder;

import java.util.LinkedList;
import java.util.List;

import config.ConfigUtils;
import util.StringUtil;

public class AudioInfoCommandBuilder {

	public List<String> makeGetAudioInfoCommand(String audioFilePath) {
		List<String> commandArgumentsList = new LinkedList<String>();

		addFFProbeExecutorPath(commandArgumentsList);
		addFilePathArgument(commandArgumentsList, audioFilePath);

		return commandArgumentsList;
	}

	private void addFFProbeExecutorPath(List<String> commandArgumentsList) {
		String doubleQuotedFFProbePath = StringUtil.makeDoubleQuotedString(ConfigUtils.FFBROBE_PATH);
		commandArgumentsList.add(doubleQuotedFFProbePath);
	}

	private void addFilePathArgument(List<String> commandArgumentsList, String audioFilePath) {
		String doubleQuotedAudioFilePath = StringUtil.makeDoubleQuotedString(audioFilePath);
		commandArgumentsList.add(doubleQuotedAudioFilePath);
	}

}

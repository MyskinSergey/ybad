package command.builder;

import java.util.LinkedList;
import java.util.List;

import config.ConfigUtils;
import model.Kid3SetCoverArguments;
import options.kid3.Kid3CommansEnum;
import options.kid3.Kid3FramesEnum;
import util.StringUtil;

public class SetCoverImageCommandBuilder {

	private static final String COMMAND_ARGUMENT = "-c";

	public List<String> makeSetCoverImageCommand(Kid3SetCoverArguments setCoverArguments) {
		String sourceImagePath = setCoverArguments.getSourceImagePath();
		String sourceAudioPath = setCoverArguments.getSourceAudioPath();

		List<String> commandArgumentsList = new LinkedList<String>();

		addKid3ExecutorPath(commandArgumentsList);
		addSetPictureOption(commandArgumentsList, sourceImagePath);
		addOutputOption(commandArgumentsList, sourceAudioPath);

		return commandArgumentsList;
	}

	private void addKid3ExecutorPath(List<String> commandArgumentsList) {
		String doubleQuotedKid3Path = StringUtil.makeDoubleQuotedString(ConfigUtils.KID3_PATH);
		commandArgumentsList.add(doubleQuotedKid3Path);
	}

	private void addSetPictureOption(List<String> commandArgumentsList, String sourceImagePath) {
		commandArgumentsList.add(COMMAND_ARGUMENT);

		StringBuilder setPictureCommandSb = new StringBuilder();

		setPictureCommandSb.append(Kid3CommansEnum.SET_TAG_FRAME.getCommand());
		setPictureCommandSb.append(" ");
		setPictureCommandSb.append(Kid3FramesEnum.PICTURE.getFrameName());
		setPictureCommandSb.append(":");
		setPictureCommandSb.append(StringUtil.makeSingleQuotedString(sourceImagePath));
		setPictureCommandSb.append(" ");
		// TODO: set picture description here if required
		String description = "";
		setPictureCommandSb.append(StringUtil.makeSingleQuotedString(description));

		commandArgumentsList.add(StringUtil.makeDoubleQuotedString(setPictureCommandSb.toString()));
	}

	private void addOutputOption(List<String> commandArgumentsList, String destinationPath) {
		String doubleQuotedDestinationPath = StringUtil.makeDoubleQuotedString(destinationPath);
		commandArgumentsList.add(doubleQuotedDestinationPath);
	}

}

package command.builder;

import java.util.LinkedList;
import java.util.List;

import config.ConfigUtils;
import model.YoutubeDlCommandArguments;
import options.youtubedl.VerbositySimulationOptionsEnum;
import options.youtubedl.VideoSelectionOptionsEnum;
import util.StringUtil;

public class VideoJsonInfoCommandBuilder {

	public List<String> makeGetVideoJsonInfoCommand(YoutubeDlCommandArguments youtubeDlCommandArguments) {
		String url = youtubeDlCommandArguments.getUrl();
		int startVideoIndex = youtubeDlCommandArguments.getStartIndex();
		int endIndex = youtubeDlCommandArguments.getEndIndex();

		List<String> commandArgumentsList = new LinkedList<String>();

		addYoutubeDlExecutorPath(commandArgumentsList);
		addSimulateOption(commandArgumentsList);
		addPrintJsonOption(commandArgumentsList);
		addPlaylistStartOption(commandArgumentsList, startVideoIndex);
		addPlaylistEndOption(commandArgumentsList, endIndex);
		addURL(commandArgumentsList, url);

		return commandArgumentsList;
	}

	private void addYoutubeDlExecutorPath(List<String> commandArgumentsList) {
		String doubleQuotedYoutubeDlPath = StringUtil.makeDoubleQuotedString(ConfigUtils.YOUTUBE_DL_PATH);
		commandArgumentsList.add(doubleQuotedYoutubeDlPath);
	}

	private void addSimulateOption(List<String> commandArgumentsList) {
		commandArgumentsList.add(VerbositySimulationOptionsEnum.SIMULATE.getOption());
	}

	private void addPrintJsonOption(List<String> commandArgumentsList) {
		commandArgumentsList.add(VerbositySimulationOptionsEnum.PRINT_JSON.getOption());
	}

	private void addPlaylistStartOption(List<String> commandArgumentsList, int startVideoIndex) {
		if (startVideoIndex > 0) {
			commandArgumentsList.add(VideoSelectionOptionsEnum.PLAYLIST_START.getOption());
			commandArgumentsList.add(String.valueOf(startVideoIndex));
		}
	}

	private void addPlaylistEndOption(List<String> commandArgumentsList, int endVideoIndex) {
		if (endVideoIndex > 0) {
			commandArgumentsList.add(VideoSelectionOptionsEnum.PLAYLIST_END.getOption());
			commandArgumentsList.add(String.valueOf(endVideoIndex));
		}
	}

	private void addURL(List<String> commandArgumentsList, String url) {
		commandArgumentsList.add(url);
	}

}

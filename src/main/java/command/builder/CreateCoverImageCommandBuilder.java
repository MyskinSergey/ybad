package command.builder;

import java.util.LinkedList;
import java.util.List;

import config.ConfigUtils;
import model.FFMpegChangeImageArguments;
import options.ffmpeg.FFMpegFilterOptionsEnum;
import options.ffmpeg.FFMpegMainOptionsEnum;
import options.ffmpeg.FFMpegVideoOptionsEnum;
import util.StringUtil;

public class CreateCoverImageCommandBuilder {

	public List<String> getScaleAspectRatioCommand(FFMpegChangeImageArguments changeImageArguments) {
		String sourceUrl = changeImageArguments.getSourceUrl();
		String destinationPath = changeImageArguments.getDestinationCoverFilePath();

		List<String> commandArgumentsList = new LinkedList<String>();

		addFFMpegExecutorPath(commandArgumentsList);
		addSourceOption(commandArgumentsList, sourceUrl);
		addOverwriteOutputFilesWithoutAskingOption(commandArgumentsList);
		addFilteringOption(commandArgumentsList, changeImageArguments);
		addOutputOption(commandArgumentsList, destinationPath);

		return commandArgumentsList;
	}

	private void addFFMpegExecutorPath(List<String> commandArgumentsList) {
		String doubleQuotedFFMpegPath = StringUtil.makeDoubleQuotedString(ConfigUtils.FFMPEG_PATH);
		commandArgumentsList.add(doubleQuotedFFMpegPath);
	}

	private void addSourceOption(List<String> commandArgumentsList, String sourceUrl) {
		commandArgumentsList.add(FFMpegMainOptionsEnum.I.getOption());
		String doubleQuotedSourcePath = StringUtil.makeDoubleQuotedString(sourceUrl);
		commandArgumentsList.add(doubleQuotedSourcePath);
	}
	
	private void addOverwriteOutputFilesWithoutAskingOption(List<String> commandArgumentsList) {
		commandArgumentsList.add(FFMpegMainOptionsEnum.Y.getOption());
	}

	private void addFilteringOption(List<String> commandArgumentsList,
			FFMpegChangeImageArguments changeImageArguments) {
		int scaleWidth = changeImageArguments.getScaleWidth();
		int scaleHeight = changeImageArguments.getScaleHeight();

		int padWidth = changeImageArguments.getPadWidth();
		int padHeight = changeImageArguments.getPadHeight();

		int padX = changeImageArguments.getPadX();
		int padY = changeImageArguments.getPadY();

		String padColor = changeImageArguments.getPadColor();

		StringBuilder filterOptionSb = new StringBuilder();

		filterOptionSb.append(FFMpegFilterOptionsEnum.SCALE.getOption());
		filterOptionSb.append("=");
		filterOptionSb.append(scaleWidth);
		filterOptionSb.append("x");
		filterOptionSb.append(scaleHeight);
		filterOptionSb.append(",");
		filterOptionSb.append(FFMpegFilterOptionsEnum.PAD.getOption());
		filterOptionSb.append("=");
		filterOptionSb.append(padWidth);
		filterOptionSb.append(":");
		filterOptionSb.append(padHeight);
		filterOptionSb.append(":");
		filterOptionSb.append(padX);
		filterOptionSb.append(":");
		filterOptionSb.append(padY);
		filterOptionSb.append(":");
		filterOptionSb.append(padColor);

		String doubleQuotedFilterOption = StringUtil.makeDoubleQuotedString(filterOptionSb.toString());
		
		commandArgumentsList.add(FFMpegVideoOptionsEnum.FILTERGRAPH.getOption());
		commandArgumentsList.add(doubleQuotedFilterOption);
	}

	private void addOutputOption(List<String> commandArgumentsList, String destinationPath) {
		String doubleQuotedDestinationPath = StringUtil.makeDoubleQuotedString(destinationPath);
		commandArgumentsList.add(doubleQuotedDestinationPath);
	}

}

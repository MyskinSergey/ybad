package command.builder;

import java.util.LinkedList;
import java.util.List;

import config.ConfigUtils;
import model.YoutubeDlCommandArguments;
import options.youtubedl.DownloadOptionsEnum;
import options.youtubedl.FilesystemOptionsEnum;
import options.youtubedl.PostProcessingOptionsEnum;
import options.youtubedl.VideoFormatOptionsEnum;
import util.StringUtil;

public class DownloadAudioCommandBuilder {

	public List<String> getDownloadAudioCommand(YoutubeDlCommandArguments youtubeDlCommandArguments) {
		String url = youtubeDlCommandArguments.getUrl();
		int formatCode = youtubeDlCommandArguments.getFormatCode();
		Integer downloadIndex = youtubeDlCommandArguments.getDownloadIndex();
		String downloadPath = youtubeDlCommandArguments.getDownloadPath();
		String fileNameTemplate = youtubeDlCommandArguments.getFileNameTemplate();

		List<String> commandArgumentsList = new LinkedList<String>();

		addYoutubeDlExecutorPath(commandArgumentsList);
		addFFMpegLocationPath(commandArgumentsList);
		addFormatOption(commandArgumentsList, formatCode);
		addExtractAudioOption(commandArgumentsList, formatCode);
		addRetriesOption(commandArgumentsList);
		addFragmentRetriesOption(commandArgumentsList);
		addOutputOption(commandArgumentsList, downloadPath, downloadIndex, fileNameTemplate);
		addURL(commandArgumentsList, url);

		return commandArgumentsList;
	}

	private void addYoutubeDlExecutorPath(List<String> commandArgumentsList) {
		String doubleQuotedYoutubeDlPath = StringUtil.makeDoubleQuotedString(ConfigUtils.YOUTUBE_DL_PATH);
		commandArgumentsList.add(doubleQuotedYoutubeDlPath);
	}

	private void addFFMpegLocationPath(List<String> commandArgumentsList) {
		commandArgumentsList.add(PostProcessingOptionsEnum.FFMPEG_LOCATION.getOption());
		String doubleQuotedFFMpegPath = StringUtil.makeDoubleQuotedString(ConfigUtils.FFMPEG_PATH);
		commandArgumentsList.add(doubleQuotedFFMpegPath);
	}

	private void addFormatOption(List<String> commandArgumentsList, int formatCode) {
		commandArgumentsList.add(PostProcessingOptionsEnum.EXTRACT_AUDIO.getOption());
	}

	private void addExtractAudioOption(List<String> commandArgumentsList, int formatCode) {
		commandArgumentsList.add(VideoFormatOptionsEnum.FORMAT.getOption());
		commandArgumentsList.add(String.valueOf(formatCode));
	}

	private void addRetriesOption(List<String> commandArgumentsList) {
		commandArgumentsList.add(DownloadOptionsEnum.RETRIES.getOption());
		commandArgumentsList.add("\"infinite\"");
	}

	private void addFragmentRetriesOption(List<String> commandArgumentsList) {
		commandArgumentsList.add(DownloadOptionsEnum.FRAGMENT_RETRIES.getOption());
		commandArgumentsList.add("\"infinite\"");
	}

	private void addOutputOption(List<String> commandArgumentsList, String downloadPath, Integer downloadIndex, String fileNameTemplate) {
		commandArgumentsList.add(FilesystemOptionsEnum.OUTPUT.getOption());

		StringBuilder fullPathSb = new StringBuilder();
		fullPathSb.append(downloadPath);
		fullPathSb.append("\\");
		if (downloadIndex != null) {
			fullPathSb.append(downloadIndex + "_");
		}
		
		fullPathSb.append(fileNameTemplate);

		String fullPath = fullPathSb.toString();
		commandArgumentsList.add(fullPath);
	}

	private void addURL(List<String> commandArgumentsList, String url) {
		commandArgumentsList.add(url);
	}

}

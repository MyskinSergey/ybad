package command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import command.builder.AudioInfoCommandBuilder;
import command.builder.CreateCoverImageCommandBuilder;
import command.builder.DownloadAudioCommandBuilder;
import command.builder.SetCoverImageCommandBuilder;
import command.builder.VideoJsonInfoCommandBuilder;
import model.FFMpegChangeImageArguments;
import model.Kid3SetCoverArguments;
import model.YoutubeDlCommandArguments;
import util.Logger;
import util.StringUtil;

public class CommandExecuter {

	public void downloadAudio(YoutubeDlCommandArguments youtubeDlCommandArguments) {
		execute(CommandTypeEnum.DOWNLOAD_AUDIO, youtubeDlCommandArguments);
	}

	public List<String> getAudioInfo(String audioFilePath) {
		List<String> resultList = execute(CommandTypeEnum.GET_AUDIO_INFO, audioFilePath);
		return resultList;
	}

	public String getVideoJsonInfo(YoutubeDlCommandArguments youtubeDlCommandArguments) {
		List<String> resultList = execute(CommandTypeEnum.GET_VIDEO_JSON_INFO, youtubeDlCommandArguments);

		StringBuilder jsonSb = new StringBuilder();
		for (String line : resultList) {
			jsonSb.append(line);
		}
		String jsonString = jsonSb.toString();

		return jsonString;
	}

	public void createCoverImage(FFMpegChangeImageArguments arguments) {
		execute(CommandTypeEnum.CREATE_COVER_IMAGE, arguments);
	}
	
	public void setCoverImage(Kid3SetCoverArguments arguments) {
		execute(CommandTypeEnum.SET_COVER_IMAGE, arguments);
	}

	private List<String> execute(CommandTypeEnum command, Object commandArgumentsObject) {
		List<String> commandArgumentsList = getArgumentsList(command, commandArgumentsObject);

		String commandStr = StringUtil.getAsSeparatedString(commandArgumentsList, StringUtil.WHITE_SPACE);
		System.out.println("Command: " + commandStr);

		List<String> resultList = new LinkedList<String>();
		Process process = null;
		Long startTime = null;

		try {
			ProcessBuilder processBuilder = new ProcessBuilder(commandArgumentsList);

			processBuilder.redirectErrorStream(true);
			process = processBuilder.start();

			startTime = System.currentTimeMillis();
			// wait for activity here

			InputStream inputStream = process.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

			String line = "";
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				Logger.info(line);

				resultList.add(line);
			}
			process.waitFor();

			System.out.println("Executing command was finished successfully!");
			in.close();
		} catch (Exception e) {
			System.out.println("Execution was interrupted!");
			e.printStackTrace();
		} finally {
			if (process.isAlive()) {
				process.destroy();
			}

			long endTime = System.currentTimeMillis();
			long seconds = (endTime - startTime) / 1000;

			System.out.println("Process takes: " + seconds + " second");
		}
		return resultList;
	}

	private List<String> getArgumentsList(CommandTypeEnum command, Object commandArgumentsObject) {
		List<String> argumentsList = null;

		switch (command) {
		case DOWNLOAD_AUDIO:
			DownloadAudioCommandBuilder downloadAudioCommandBuilder = new DownloadAudioCommandBuilder();
			YoutubeDlCommandArguments downloadAudioCommandArguments = (YoutubeDlCommandArguments) commandArgumentsObject;
			argumentsList = downloadAudioCommandBuilder.getDownloadAudioCommand(downloadAudioCommandArguments);
			break;
		case GET_AUDIO_INFO:
			AudioInfoCommandBuilder audioInfoCommandBuilder = new AudioInfoCommandBuilder();
			String getAudioInfoCommandArguments = (String) commandArgumentsObject;
			argumentsList = audioInfoCommandBuilder.makeGetAudioInfoCommand(getAudioInfoCommandArguments);
			break;
		case GET_VIDEO_JSON_INFO:
			VideoJsonInfoCommandBuilder videoJsonInfoCommandBuilder = new VideoJsonInfoCommandBuilder();
			YoutubeDlCommandArguments videoJsonInfoCommandArguments = (YoutubeDlCommandArguments) commandArgumentsObject;
			argumentsList = videoJsonInfoCommandBuilder.makeGetVideoJsonInfoCommand(videoJsonInfoCommandArguments);
			break;
		case CREATE_COVER_IMAGE:
			CreateCoverImageCommandBuilder createCoverImageCommandBuilder = new CreateCoverImageCommandBuilder();
			FFMpegChangeImageArguments ffMpegChangeImageArguments = (FFMpegChangeImageArguments) commandArgumentsObject;
			argumentsList = createCoverImageCommandBuilder.getScaleAspectRatioCommand(ffMpegChangeImageArguments);
			break;
		case SET_COVER_IMAGE:
			SetCoverImageCommandBuilder setCoverImageCommandBuilder = new SetCoverImageCommandBuilder();
			Kid3SetCoverArguments kid3SetCoverArguments = (Kid3SetCoverArguments) commandArgumentsObject;
			argumentsList = setCoverImageCommandBuilder.makeSetCoverImageCommand(kid3SetCoverArguments);
			break;
		default:
			System.out.println("No command found!");
			break;
		}
		return argumentsList;
	}

}

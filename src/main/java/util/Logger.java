package util;

import java.io.PrintWriter;
import java.io.StringWriter;

import view.MainWindow;

public class Logger {

	private static MainWindow mainWindow;

	public static void info(String string) {
		System.out.println(string);
		mainWindow.addTextData(string);
	}

	public static void error(Throwable ex) {
		// TODO: Remove all duplicated printing stack traces before executing this method
		ex.printStackTrace();
		
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		String exceptionDetails = sw.toString();

		String[] exDetailsStrings = exceptionDetails.split("\\\n");
		for (String line : exDetailsStrings) {
			mainWindow.addTextData(line);
		}
	}

	public void setMainWindow(MainWindow mainWindow) {
		Logger.mainWindow = mainWindow;
	}

}

package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtils {

	private static final String LATIN_SYMBOLS_ONLY_WINDOWS_PATH_REGEXP = "^[A-Za-z]:\\\\(?:[\\w\\s\\!,@#~`';&%\\$\\^\\(\\)\\{\\}\\[\\]=\\+\\-\\.]+\\\\?)*\\\\?$";
	private static final String LATIN_SYMBOLS_ONLY_FILE_NAME_REGEXP = "^[\\w\\s\\!,@#~`';&%\\$\\^\\(\\)\\{\\}\\[\\]=\\+\\-\\.]+$";

	private static Pattern latinSymbolsOnlyWindowsPathPattern;
	private static Pattern latinSymbolsOnlyFileNamePattern;

	static {
		latinSymbolsOnlyWindowsPathPattern = Pattern.compile(LATIN_SYMBOLS_ONLY_WINDOWS_PATH_REGEXP);
		latinSymbolsOnlyFileNamePattern = Pattern.compile(LATIN_SYMBOLS_ONLY_FILE_NAME_REGEXP);
	}

	public static boolean renameFile(String sourceFileAbsolutePath, String destinationFileAbsolutePath) {
		File sourceFile = new File(sourceFileAbsolutePath);

		File renamedFile = new File(destinationFileAbsolutePath);
		boolean renameResult = sourceFile.renameTo(renamedFile);

		if (!renameResult) {
			Logger.info("Can't rename file!");
		}

		return renameResult;
	}

	public static String getFileExtension(String filePath) {
		int beginIndex = filePath.lastIndexOf(".") + 1;

		String extension = filePath.substring(beginIndex);
		return extension;
	}

	public static File getSystemTempFolder() {
		String tempDirPath = System.getProperty("java.io.tmpdir");
		File tempFolderDir = new File(tempDirPath);
		return tempFolderDir;
	}

	// TODO: Add method for getting non latin temp path: system folder, C:\\
	// or folder of Kid3 tool for example
	public static boolean isLatinSymbolsOnlyWindowsPath(String path) {
		Matcher matcher = latinSymbolsOnlyWindowsPathPattern.matcher(path);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	public static boolean isLatinSymbolsOnlyFileName(String fileName) {
		Matcher matcher = latinSymbolsOnlyFileNamePattern.matcher(fileName);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	public static boolean moveFile(String sourcePathStr, String destinationPathStr) {
		Path sourcePath = Paths.get(sourcePathStr);
		Path destinationPath = Paths.get(destinationPathStr);

		try {
			Files.move(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
			return true;
		} catch (IOException e) {
			Logger.info("Moving file failed!");
			Logger.error(e);
		}
		return false;
	}

	public static boolean removeFile(String filePath) {
		File file = new File(filePath);
		boolean isDeleted = file.delete();
		return isDeleted;
	}

}

package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtils {

	private static String DURATION_HH_MM_SS_TIME_FORMAT = "%d:%02d:%02d";
	private static String DURATION_MM_SS_TIME_FORMAT = "%d:%02d";

	private static String YYYYMMDD_DATE_PATTERN = "yyyyMMdd";
	private static String DDMMYYYY_DATE_PATTERN = "dd.MM.yyyy";
	private static String DDMMYY_DATE_PATTERN = "dd.MM.yy";

	public static String getDurationAsHHMMSS(Integer duration) {
		Long millis = duration * 1000L;

		long hours = TimeUnit.MILLISECONDS.toHours(millis);
		millis -= TimeUnit.HOURS.toMillis(hours);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
		millis -= TimeUnit.MINUTES.toMillis(minutes);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

		String durationStr = "";
		if (hours > 0) {
			durationStr = String.format(DURATION_HH_MM_SS_TIME_FORMAT, hours, minutes, seconds);
		} else {
			durationStr = String.format(DURATION_MM_SS_TIME_FORMAT, minutes, seconds);
		}
		return durationStr;
	}

	public static Date parseUploadDate(String dateStr) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_DATE_PATTERN);
		try {
			Date uploadDate = simpleDateFormat.parse(dateStr);
			return uploadDate;
		} catch (ParseException e) {
			e.printStackTrace();
			Logger.error(e);
			Logger.info("Can't parse date '" + dateStr + "' using pattern '" + YYYYMMDD_DATE_PATTERN + "'");
		}
		return null;
	}

	public static String dateToDDMMYYYY(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DDMMYYYY_DATE_PATTERN);
		String formatedDate = simpleDateFormat.format(date);
		return formatedDate;
	}

	public static String dateToDDMMYY(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DDMMYY_DATE_PATTERN);
		String formatedDate = simpleDateFormat.format(date);
		return formatedDate;
	}

	public static boolean isStringContainsDate(String string, Date date) {

		String dateDDMMYY = dateToDDMMYY(date);
		if (string.contains(dateDDMMYY)) {
			return true;
		}
		String dateDDMMYYYY = dateToDDMMYYYY(date);
		if (string.contains(dateDDMMYYYY)) {
			return true;
		}
		String dateDDMMYYwithSlashes = dateDDMMYY.replace(".", "/");
		if (string.contains(dateDDMMYYwithSlashes)) {
			return true;
		}
		String dateDDMMYYwithBackSlashes = dateDDMMYY.replace(".", "\\");
		if (string.contains(dateDDMMYYwithBackSlashes)) {
			return true;
		}
		String dateDDMMYYYYwithSlashes = dateDDMMYYYY.replace(".", "/");
		if (string.contains(dateDDMMYYYYwithSlashes)) {
			return true;
		}
		String dateDDMMYYYYwithBackSlashes = dateDDMMYYYY.replace(".", "\\");
		if (string.contains(dateDDMMYYYYwithBackSlashes)) {
			return true;
		}

		return false;
	}

}

package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YoutubeUtil {

	// TODO: Fix youtube video URL regexp for understanding URL's like
	// https://www.youtube.com/watch?feature=player_embedded&v=WKVJxHkrlEQ
	// Check should it be double quoted or not
	private static final String YOUTUBE_VIDEO_URL_REGEXP = "^https?\\://www\\.youtube\\.com/watch\\?v=(\\w|-){11}$";
	private static final String YOUTUBE_PLAYLIST_URL_REGEXP = "^https?\\://www\\.youtube\\.com/playlist\\?list=(?:\\w|-)*$";
	private static final String YOUTUBE_CHANNEL_URL_REGEXP = "^(?:https?\\://)?(?:www\\.)?youtube\\.com/(?:channel|user)/(?:(?:\\w|-){1,}/?){1,}$";

	private static Pattern youtubeVideoUrlRegexpPattern;
	private static Pattern youtubePlaylistUrlRegexpPattern;
	private static Pattern youtubeChannelUrlRegexpPattern;

	static {
		youtubeVideoUrlRegexpPattern = Pattern.compile(YOUTUBE_VIDEO_URL_REGEXP);
		youtubePlaylistUrlRegexpPattern = Pattern.compile(YOUTUBE_PLAYLIST_URL_REGEXP);
		youtubeChannelUrlRegexpPattern = Pattern.compile(YOUTUBE_CHANNEL_URL_REGEXP);
	}

	public static boolean isVideoUrl(String line) {
		Matcher matcher = youtubeVideoUrlRegexpPattern.matcher(line);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	public static boolean isPlaylistUrl(String line) {
		Matcher matcher = youtubePlaylistUrlRegexpPattern.matcher(line);
		boolean matchResult = matcher.matches();
		return matchResult;
	}

	public static boolean isChannelUrl(String line) {
		Matcher matcher = youtubeChannelUrlRegexpPattern.matcher(line);
		boolean matchResult = matcher.matches();
		return matchResult;
	}

}

package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpUtil {

	private static final String AUDIO_INFO_BITRATE_REGEXP = "^.*bitrate\\:\\s+(\\d+)\\s+kb/s$";
	private static final String AUDIO_FILE_TO_COMPARE_NAME_REGEXP = "^\\d+_.*\\.\\w{3,4}$";

	private static Pattern audioInfoBitratePattern;
	private static Pattern audioFileToCompareNamePattern;

	static {
		audioInfoBitratePattern = Pattern.compile(AUDIO_INFO_BITRATE_REGEXP);
		audioFileToCompareNamePattern = Pattern.compile(AUDIO_FILE_TO_COMPARE_NAME_REGEXP);
	}

	public static Integer getBitRate(String line) {
		Matcher matcher = audioInfoBitratePattern.matcher(line);
		if (matcher.matches()) {
			String bitrateStr = matcher.group(1);
			Integer bitrate = Integer.valueOf(bitrateStr);
			return bitrate;
		}
		return null;
	}

	public static boolean isAudioToCompareFileName(String name) {
		Matcher matcher = audioFileToCompareNamePattern.matcher(name);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

}

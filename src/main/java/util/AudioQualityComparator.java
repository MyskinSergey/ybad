package util;

import java.util.Comparator;

import entity.AudioQuality;
import options.AudioFormatEnum;

public class AudioQualityComparator implements Comparator<AudioQuality> {

	@Override
	public int compare(AudioQuality aq1, AudioQuality aq2) {
		int compareSizeResult = compareSize(aq1, aq2);
		if (compareSizeResult != 0) {
			return compareSizeResult;
		}

		Integer bitrate1 = aq1.getBitrate();
		Integer bitrate2 = aq2.getBitrate();

		if ((bitrate1 == null) && (bitrate2 == null)) {
			return 0;
		} else if ((bitrate1 != null) && (bitrate2 == null)) {
			return 1;
		} else if ((bitrate1 == null) && (bitrate2 != null)) {
			return -1;
		}

		if (!bitrate1.equals(bitrate2)) {
			int bitratesCompareResult = bitrate1.compareTo(bitrate2);
			return bitratesCompareResult;
		} else {
			AudioFormatEnum audioFormat1 = aq1.getAudioFormat();
			AudioFormatEnum audioFormat2 = aq2.getAudioFormat();

			Integer qualityPriority1 = audioFormat1.getQualityPriority();
			Integer qualityPriority2 = audioFormat2.getQualityPriority();

			int qualityCompareResult = qualityPriority1.compareTo(qualityPriority2);
			return qualityCompareResult;
		}
	}

	private int compareSize(AudioQuality aq1, AudioQuality aq2) {
		Long size1 = aq1.getSize();
		Long size2 = aq2.getSize();

		if ((size1 != null) && (size2 != null)) {
			int sizeCompareResult = size1.compareTo(size2);
			return sizeCompareResult;
		}
		return 0;
	}

}

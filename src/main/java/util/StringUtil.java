package util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

public class StringUtil {

	public static final String WHITE_SPACE = " ";

	public static String makeDoubleQuotedString(String stringToQoute) {
		String quotedString = "\"" + stringToQoute + "\"";
		return quotedString;
	}

	public static String makeSingleQuotedString(String stringToQoute) {
		String quotedString = "'" + stringToQoute + "'";
		return quotedString;
	}

	public static String getAsSeparatedString(List<String> strList, String separator) {
		StringBuilder sb = new StringBuilder();
		Iterator<String> iterator = strList.iterator();

		while (iterator.hasNext()) {
			String string = (String) iterator.next();
			sb.append(string);
			if (iterator.hasNext()) {
				sb.append(separator);
			}
		}
		return sb.toString();
	}

	public static String encodeToUTF8(String inputString) {
		Charset cset = Charset.forName("UTF-8");
		ByteBuffer buf = cset.encode(inputString);
		byte[] b = buf.array();
		String str = new String(b);
		return str;
	}

	public static String encodeToUTF8_2(String inputString) {
		return inputString;

	}

}
